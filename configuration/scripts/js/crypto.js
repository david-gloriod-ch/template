const crypto = require('crypto');

const { privateKey, publicKey } = crypto.generateKeyPairSync('ec', {
	namedCurve: 'secp521r1'
});

const privateKeyString = privateKey.export({ type: 'pkcs8', format: 'pem' }).toString();
const publicKeyString = publicKey.export({ type: 'spki', format: 'pem' }).toString();

function generateKey(length = 64) {
    return new Array(length).fill(0).map(_ => _ = String.fromCharCode(parseInt(Math.random() * 10000))).join('')
}

console.log(`JWT_PRIVATE_KEY=${privateKeyString.trim('\n').replaceAll('\n', '\\n').replaceAll('=', '{EQUALS}')}`);
console.log(`JWT_PUBLIC_KEY=${publicKeyString.trim('\n').replaceAll('\n', '\\n').replaceAll('=', '{EQUALS}')}`);
console.log(`APP_KEY=${generateKey()}`);
console.log(`SALT_PASSWORD=${generateKey()}`);
console.log(`SALT_SESSION_ID=${generateKey()}`);
console.log(`POSTGRES_PASSWORD=${crypto.randomBytes(24).toString('hex')}`);
console.log(`PGADMIN_DEFAULT_PASSWORD=${crypto.randomBytes(24).toString('hex')}`);