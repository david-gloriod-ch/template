import { MiddlewareConsumer, Module, RequestMethod } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UsersModule } from './users/users.module';
import * as dotenv from 'dotenv';
import { PrismaService } from './prisma.service';
import { SessionsModule } from './sessions/sessions.module';
import { SessionsMiddleware } from './sessions/sessions.middleware';
import { CookieService } from './core/services/cookie.service';
import { AuthModule } from './auth/auth.module';
import { AuthMiddleware } from './auth/auth.middleware';
import { MailModule } from './mail/mail.module';
import { ProfileModule } from './profile/profile.module';
import { APP_GUARD } from '@nestjs/core';
import { AuthGuard } from './auth/auth.guard';
import { LoggerModule } from './logger/logger.module';
import { LoggerMiddleware } from './logger/logger.middleware';
import { PermissionsModule } from './permissions/permissions.module';
import { PermissionGuard } from './permissions/permission.guard';
import { RolesModule } from './roles/roles.module';
import { ExampleModule } from './example/example.module';
import { StripeModule } from './stripe/stripe.module';
import { StripeMiddleware } from './stripe/stripe.middleware';
import { WebhookModule } from './webhook/webhook.module';
import { WebhookMiddleware } from './webhook/webhook.middleware';
import { CSRFMiddleware } from './security/middleware/csrf.middleware';

dotenv.config();

@Module({
	imports: [
		UsersModule,
		SessionsModule,
		AuthModule,
		MailModule,
		ProfileModule,
		LoggerModule,
		PermissionsModule,
		RolesModule,
		ExampleModule,
		StripeModule,
		WebhookModule,
	],
	controllers: [AppController],
	providers: [
		AppService,
		PrismaService,
		CookieService,
		{
			provide: APP_GUARD,
			useClass: AuthGuard
		},
		{
			provide: APP_GUARD,
			useClass: PermissionGuard
		},
	],
})
export class AppModule {
	constructor(private readonly app: AppService) {
		this.app.init();
	}
	configure(consumer: MiddlewareConsumer) {
		consumer.apply(
			WebhookMiddleware,
			StripeMiddleware,
			LoggerMiddleware,
			CSRFMiddleware,
			SessionsMiddleware,
			AuthMiddleware,
		).forRoutes('*');
	}
}
