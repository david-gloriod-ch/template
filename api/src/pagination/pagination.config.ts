import { skip } from "node:test";

const limit = 5;

export const PAGINATION_CONFIG = {
	take: limit,
	skip: (page: number) => limit * (page - 1),
}
