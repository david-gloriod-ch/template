import { Injectable } from '@nestjs/common';

type mask = 'create' | 'read' | 'update' | 'delete' | 'all';

@Injectable()
export class PermissionCheckerService {

	private readonly DEFAULT_ACCESS: string = "default-access";

	private getMask(type: mask) {
		switch (type) {
			case 'create':
				return parseInt('1000', 2);
			case 'read':
				return parseInt('0100', 2);
			case 'update':
				return parseInt('0010', 2);
			case 'delete':
				return parseInt('0001', 2);
			case 'all':
				return parseInt('1111', 2);
			default:
				return 0;
		}
	}

	getPermissionFromMethod(method: string) {
		switch (method) {
			case 'POST':
				return 'create';
			case 'GET':
				return 'read';
			case 'PUT':
				return 'update';
			case 'DELETE':
				return 'delete';
			default:
				return '';
		}
	}

	checkPermission(permissions: {}, permission: string) {
		const infos = permission.split(':');
		const name = infos[0];
		const level = infos[1];
		const mask = this.getMask(level as mask);

		return permissions[name] & mask || permissions[this.DEFAULT_ACCESS];
	}
}
