import { CanActivate, ExecutionContext, ForbiddenException, Injectable, Optional, UnauthorizedException } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import * as jwt from 'jsonwebtoken';
import { Request } from 'express';
import * as CryptoJS from 'crypto-js';
import { HAS_PERMISSION_KEY } from './decorators/permission.decorator';
import { PermissionCheckerService } from './permissions-checker.service';

type mask = 'create' | 'read' | 'update' | 'delete';
const DEFAULT_ACCESS = "default-access";

@Injectable()
export class PermissionGuard implements CanActivate {
	constructor(
		private readonly reflector: Reflector,
		private readonly permissionCheckerService: PermissionCheckerService
	) {}

	async canActivate(_context: ExecutionContext): Promise<boolean> {
		const request: Request = _context.switchToHttp().getRequest();
		const hasPermission = this.reflector.get<string>(HAS_PERMISSION_KEY, _context.getHandler());

		if (!hasPermission)
			return true;

		if (!this.permissionCheckerService.checkPermission(request['user_infos'].permissions, hasPermission[0]))
			throw new ForbiddenException('Action refusée');
		return true;
	}
}
