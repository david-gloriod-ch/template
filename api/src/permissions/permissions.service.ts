import { Inject, Injectable } from '@nestjs/common';
import { CreatePermissionDto } from './dto/create-permission.dto';
import { UpdatePermissionDto } from './dto/update-permission.dto';
import { BaseService } from 'src/generics/service/base.service';
import { Prisma } from '@prisma/client';
import { REQUEST } from '@nestjs/core';
import { Request } from 'express';
import { PAGINATION_CONFIG } from 'src/pagination/pagination.config';

@Injectable()
export class PermissionsService extends BaseService<Prisma.PermissionDelegate> {

	constructor(
		@Inject(REQUEST)
		protected readonly request: Request
	) {
		super(request);
		this.entity = this.prisma.permission;
	}

	async create(datas: CreatePermissionDto) {
		return await this.entity.create({ data: datas });
	}

	async findAll(page: number) {
		const permissions  = await this.entity.findMany({
			take: PAGINATION_CONFIG.take,
			skip: PAGINATION_CONFIG.skip(page),
			orderBy: { name: 'asc' }
		});
		const total = await this.entity.count();

		return {
			permissions,
			total
		};
	}

	async findUsers(id: string, page: number) {
		let permission = null;
		let roles = null;
		let total = null;

		// permission = await this.entity.findUnique({
		// 	where: { id },
		// 	select: {
		// 		roles: {
		// 			where: {
		// 				unique_user: true
		// 			},
		// 			select: {
		// 				users: true
		// 			}
		// 		}
		// 	}
		// });
		// roles = await this.prisma.role.findMany({
		// 	where: {
		// 		unique_user: true
		// 	},
		// 	include: {
		// 		users: true
		// 	}
		// });
		// total = await this.prisma.user.count({});

		return {
			permission,
			users: roles,
			total
		};
	}

	async findOne(id: string) {
		return await this.entity.findUnique({
			where: { id },
			include: {
				roles: {
					include: {
						permission: true
					}
				}
			}
		});
	}

	async update(id: string, datas: UpdatePermissionDto) {
		return await this.entity.update({ where: { id }, data: datas });
	}

	async remove(id: string) {
		return await this.entity.delete({ where: { id } });
	}
}
