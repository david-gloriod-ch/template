import { IsNotEmpty, IsString } from "class-validator";

export class CreatePermissionDto {
	@IsString({
		message: 'Le titre doit être une chaîne de caractères.'
	})
	@IsNotEmpty({
		message: 'Le titre est obligatoire.'
	})
	title: string;

	@IsString({
		message: 'Le nom doit être une chaîne de caractères.'
	})
	@IsNotEmpty({
		message: 'Le nom est obligatoire.'
	})
	name: string;

	@IsString({
		message: 'La description doit être une chaîne de caractères.'
	})
	@IsNotEmpty({
		message: 'La description est obligatoire.'
	})
	description: string;
}
