import { Controller, Get, Post, Body, Patch, Param, Delete, Query } from '@nestjs/common';
import { PermissionsService } from './permissions.service';
import { CreatePermissionDto } from './dto/create-permission.dto';
import { UpdatePermissionDto } from './dto/update-permission.dto';
import { Permission } from './decorators/permission.decorator';
import { PagePipe } from 'src/generics/pipes/page.pipe';

@Controller('permissions')
export class PermissionsController {
	constructor(private readonly permissionsService: PermissionsService) {}
	
	@Permission('permission:create')
	@Post()
	create(@Body() datas: CreatePermissionDto) {
		return this.permissionsService.create(datas);
	}

	// @Permission('permission:read')
	@Get()
	findAll(@Query('page', PagePipe) page: number) {
		return this.permissionsService.findAll(page);
	}

	// @Permission('permission:read')
	@Get(':id/users')
	findUsers(@Param('id') id: string, @Query('page', PagePipe) page: number) {
		return this.permissionsService.findUsers(id, page);
	}

	// @Permission('permission:read')
	@Get(':id')
	findOne(@Param('id') id: string) {
		return this.permissionsService.findOne(id);
	}

	// @Permission('permission:update')
	@Patch(':id')
	update(@Param('id') id: string, @Body() datas: UpdatePermissionDto) {
		return this.permissionsService.update(id, datas);
	}

	// @Permission('permission:delete')
	@Delete(':id')
	remove(@Param('id') id: string) {
		return this.permissionsService.remove(id);
	}
}
