import { Module } from '@nestjs/common';
import { PermissionsService } from './permissions.service';
import { PermissionsController } from './permissions.controller';
import { PermissionCheckerService } from './permissions-checker.service';

@Module({
	controllers: [PermissionsController],
	providers: [
		PermissionsService,
		PermissionCheckerService,
	],
	exports: [
		PermissionsService,
		PermissionCheckerService
	]
})
export class PermissionsModule {}
