
import { SetMetadata } from '@nestjs/common';

export const HAS_PERMISSION_KEY = 'permission';
export const Permission = (...permissions: string[]) => SetMetadata(HAS_PERMISSION_KEY, permissions);