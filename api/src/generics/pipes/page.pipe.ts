import { ArgumentMetadata, Injectable, NotFoundException, PipeTransform } from '@nestjs/common';

@Injectable()
export class PagePipe implements PipeTransform {
	transform(_page: string, metadata: ArgumentMetadata) {
		const page = parseInt(_page);
		if (isNaN(page) || page < 1 || page > 2000000000 || !isFinite(page))
			throw new NotFoundException({ status: 404, message: 'Numéro de page invalide' });
		return page;
	}
}
