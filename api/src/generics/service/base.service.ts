import { Inject, Injectable } from '@nestjs/common';
import { REQUEST } from '@nestjs/core';
import { Prisma } from '@prisma/client';
import { Request } from 'express';
import { IDatasPayload } from 'src/auth/interfaces/payload';
import { PrismaService } from 'src/prisma.service';

@Injectable()
export abstract class BaseService<T> {
	private _entity: T;

	constructor(
		@Inject(REQUEST)
		protected readonly request: Request
	) {}

	get prisma(): PrismaService {
		return PrismaService.prisma;
	}

	get entity(): T {
		return this._entity;
	}

	protected set entity(value: T) {
		this._entity = value;
	}

	get user(): Prisma.UserWhereUniqueInput {
		return this.request['user_infos'].user;
	}

	get session(): Prisma.SessionWhereUniqueInput {
		return this.request['user_infos'].session;
	}

	get user_infos(): IDatasPayload {
		return this.request['user_infos'];
	}
}