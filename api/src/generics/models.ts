import { PrismaClient } from "@prisma/client";
import { PrismaService } from "src/prisma.service";

export class Model<T> {
	private _entity: T;
	readonly id: string;
	readonly createdAt: Date;
	updatedAt: Date;

	constructor(id: string, createdAt: Date, updatedAt: Date) {
		this.id = id;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
	}

	get prisma(): PrismaClient {
		return PrismaService.prisma;
	}

	protected set entity(value: T) {
		this._entity = value;
	}

	protected get entity(): T {
		return this._entity;
	}

	protected toJSON() {
		return {
			id: this.id,
			createdAt: this.createdAt,
			updatedAt: this.updatedAt
		}
	}
}
