import { MailerOptions } from '@nestjs-modules/mailer';
import * as dotenv from 'dotenv';

dotenv.config();

const config: MailerOptions = {
	transport: {
		host: process.env.MAIL_HOST,
		port: parseInt(process.env.MAIL_PORT),
		secure: true,
		auth: {
			user: process.env.MAIL_FROM,
			pass: process.env.MAIL_PASSWORD,
		},
	},
	defaults: {
		from: `${process.env.APP_NAME} <${process.env.MAIL_FROM}>`,
	},
};

export const mailerConfig = config;
