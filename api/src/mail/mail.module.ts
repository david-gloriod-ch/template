import { Module } from '@nestjs/common';
import { MailService } from './mail.service';
import { mailerConfig } from './mail.config';
import { MailerModule } from '@nestjs-modules/mailer';

@Module({
	imports: [
		MailerModule.forRoot(mailerConfig),
	],
	providers: [MailService],
	exports: [MailService],
})
export class MailModule {}
