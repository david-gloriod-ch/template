import { Injectable, NotFoundException } from '@nestjs/common';
import { BaseService } from 'src/generics/service/base.service';
import { SendMailDto } from './dtos/send-mail.dto';
import { MailerService } from '@nestjs-modules/mailer';

@Injectable()
export class MailService {
	constructor(
		private readonly mailerService: MailerService,
	) {}

	async sendMail(datas: SendMailDto)
	{
		const mail = await this.mailerService.sendMail({
			from: datas.from ?? process.env.MAIL_FROM, //'process.env.MAIL_FROM',
			to: process.env.MAIL_TO ?? datas.to, //'process.env.MAIL_FROM',
			cc: process.env.MAIL_TO ?? datas.cc,
			subject: datas.subject,
			html: `
				${datas.text}
				<br/>
				<p>Ceci est un mail automatique, merci de ne pas y répondre.</p>
			`
		});
	}
}
