import { Controller, Get, Post, Body, Patch, Param, Delete, Res, Req, Query } from '@nestjs/common';
import { UsersService } from './users.service';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { Request, Response } from 'express';
import { IdPipe } from 'src/generics/pipes/id.pipe';
import { AddRoleDto } from './dto/add-role.dto';
import { RemoveRoleDto } from './dto/remove-role.dto';
import { PagePipe } from 'src/generics/pipes/page.pipe';
import { SearchDto } from './dto/search.dto';

@Controller('users')
export class UsersController {
	constructor(
		private readonly service: UsersService
	) {}

	@Post()
	create(@Body() createUserDto: CreateUserDto) {
		return this.service.create(createUserDto);
	}

	@Get()
	async findAll(@Query('page', PagePipe) page: number) {
		return this.service.findAll(page);
	}

	@Get('roles')
	async fetchRoles(@Query('page') page: number) {
		return await this.service.fetchRoles(page);
	}

	@Get('search')
	async search(@Query() search: SearchDto) {
		return await this.service.search(search);
	}

	@Get(':id/role')
	fetchRole(@Param('id', IdPipe) id: string) {
		return this.service.fetchRole(id);
	}

	@Get(':id/roles')
	fetchRolesUser(@Param('id', IdPipe) id: string, @Query('page', PagePipe) page: number) {
		return this.service.fetchRolesUser(id, page);
	}

	@Get(':id/permissions')
	fetchPermissions(@Param('id', IdPipe) id: string) {
		return this.service.fetchPermissions(id);
	}

	@Get(':id')
	findOne(@Param('id', IdPipe) id: string) {
		return this.service.findOne(id);
	}

	@Post(':id/role')
	addRole(@Param('id', IdPipe) id: string, @Body() datas: AddRoleDto) {
		return this.service.addRole(id, datas);
	}

	@Delete(':id/role')
	removeRole(@Param('id', IdPipe) id: string, @Body() datas: RemoveRoleDto) {
		return this.service.removeRole(id, datas);
	}

	@Patch(':id')
	update(@Param('id', IdPipe) id: string, @Body() updateUserDto: UpdateUserDto) {
		return this.service.update(id, updateUserDto);
	}

	@Delete(':id')
	remove(@Param('id', IdPipe) id: string) {
		return this.service.remove(id);
	}
}
