import { IsNotEmpty, IsUUID } from "class-validator";

export class AddRoleDto {
	@IsUUID('4', { message: 'Role invalide' })
	@IsNotEmpty({
		message: 'Role nécessaire'
	})
	readonly roleId: string;
}