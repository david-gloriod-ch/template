import { IsNotEmpty, IsNumber, IsString, Max, Min, Validate } from "class-validator";
import { PagePipe } from "src/generics/pipes/page.pipe";

export class SearchDto {
	@IsString({
		message: 'Email invalide'
	})
	@IsNotEmpty({
		message: 'Email nécessaire'
	})
	readonly email: string;

	@Validate(PagePipe)
	@IsNotEmpty({
		message: 'Page nécessaire'
	})
	readonly page: number;
}
