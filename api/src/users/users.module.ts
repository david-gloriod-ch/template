import { Module, forwardRef } from '@nestjs/common';
import { UsersService } from './users.service';
import { UsersController } from './users.controller';
import { StripeModule } from 'src/stripe/stripe.module';

@Module({
	imports: [
		forwardRef(() => StripeModule)
	],
	controllers: [UsersController],
	providers: [
		UsersService,
	],
	exports: [
		UsersService
	]
})
export class UsersModule {}
