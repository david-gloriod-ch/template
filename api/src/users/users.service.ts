import { ConflictException, Inject, Injectable, NotFoundException } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { PrismaService } from 'src/prisma.service';
import { BaseService } from 'src/generics/service/base.service';
import { Prisma } from '@prisma/client';
import { REQUEST } from '@nestjs/core';
import { Request } from 'express';
import { AddRoleDto } from './dto/add-role.dto';
import { RemoveRoleDto } from './dto/remove-role.dto';
import { SearchDto } from './dto/search.dto';
import { PAGINATION_CONFIG } from 'src/pagination/pagination.config';
import { stripe } from 'src/stripe/stripe';
import { StripeCustomerService } from 'src/stripe/customers/service';
import { AddPermissionDto } from 'src/roles/dto/add-permission.dto';
import { User } from './users.model';

@Injectable()
export class UsersService extends BaseService<Prisma.UserDelegate> {

	constructor(
		@Inject(REQUEST)
		protected readonly request: Request,
		private readonly stripeCustomerService: StripeCustomerService
	)
	{
		super(request);
		this.entity = this.prisma.user;
	}

	async create(datas: CreateUserDto) {
		const user = await this.findByEmail(datas.email);
		if (user)
			throw new ConflictException('Adresse email déjà existante');

		const created = await this.entity.create({
			data: datas
		});

		await this.prisma.userInfos.create({
			data: {
				userId: created.id,
				number_of_sessions: 0
			}
		});

		const customer = await this.stripeCustomerService.create(created);

		await this.entity.update({
			where: {
				id: created.id
			},
			data: {
				stripeCustomer: {
					connect: { id: customer.id }
				}
			}
		});

		await this.prisma.role.create({
			data: {
				name: created.id,
				title: created.id,
				description: `Role utilisateur de ${created.id}`,
				unique_user: true,
				users: {
					connect: {
						id: created.id
					}
				}
			}
		});
		return created;
	}

	async findAll(page: number) {
		const users = await this.entity.findMany({
			where: {
				email: { not: process.env.DEFAULT_USER_EMAIL }
			},
			take: PAGINATION_CONFIG.take,
			skip: PAGINATION_CONFIG.skip(page),
			orderBy: { email: 'asc' }
		});
		const total = await this.entity.count();
		return {
			users,
			total
		}
	}

	async fetchRoles(page: number) {
		const users = await this.entity.findMany({
			where: {
				email: {
					not: process.env.DEFAULT_USER_EMAIL
				}
			},
			take: PAGINATION_CONFIG.take,
			skip: PAGINATION_CONFIG.skip(page),
			orderBy: { email: 'asc' },
			include: {
				roles: true
			}
		});
		const total = await this.entity.count();

		return {
			users,
			total
		};
	}

	async findOne(id: string) {
		return await this.entity.findFirst({
			where: {
				id: id
			},
		});
	}

	async fetchRole(id: string) {
		const role = await this.entity.findUnique({
			where: {
				id: id
			},
			include: {
				roles: {
					where: {
						unique_user: true
					},
				}
			},
		});

		if (!role)
			throw new NotFoundException('Utilisateur introuvable');

		return role.roles[0];
	}

	async fetchRolesUser(id: string, page: number) {
		const roles = await this.entity.findUnique({
			where: {
				id: id
			},
			include: {
				roles: {
					where: {
						unique_user: false
					},
					take: PAGINATION_CONFIG.take,
					skip: PAGINATION_CONFIG.skip(page),
				},
			},
		});

		if (!roles)
			throw new NotFoundException('Utilisateur introuvable');

		return roles.roles;
	}

	async fetchPermissions(id: string) {
		return await this.entity.findUnique({
			where: {
				id: id
			},
			include: {
				roles: {
					where: {
						unique_user: true
					},
					include: {
						permissions: true
					}
				}
			},
		});
	}

	async search(search: SearchDto) {
		const where = {
			AND: [
				{
					email: {
						contains: search.email
					},
				},
				{
					email: {
						not: process.env.DEFAULT_USER_EMAIL
					}
				}
			]
		};

		const users = await this.entity.findMany({
			where: where,
			orderBy: {
				email: 'asc'
			},
			take: PAGINATION_CONFIG.take,
			skip: PAGINATION_CONFIG.skip(search.page)
		});
		const total = await this.entity.count({
			where: where
		});

		return {
			users,
			total
		};
	}

	async addPermission(id: string, datas: AddPermissionDto) {
		const user = await this.entity.findUnique({
			where: {
				id: id
			},
			include: {
				roles: {
					where: {
						unique_user: true
					},
				}
			},
		});

		if (!user)
			throw new NotFoundException('Utilisateur introuvable');

		if (!await this.prisma.permission.findUnique({ where: { id: datas.permissionId } }))
			throw new NotFoundException('Permission introuvable');

		return await this.prisma.rolePermissionLevel.upsert({
			where: {
				roleId_permissionId: {
					roleId: user.roles[0].id,
					permissionId: datas.permissionId
				}
			},
			update: {},
			create: {
				roleId: user.roles[0].id,
				permissionId: datas.permissionId,
				level: parseInt(datas.level)
			}
		});
	}

	async findByEmail(email: string): Promise<Prisma.UserWhereInput> {
		return await this.entity.findUnique({
			where: {
				email: email
			}
		});
	}

	async findFirstOrCreate(datas: Prisma.UserCreateInput) {
		const user = await this.entity.findFirst({
			where: {
				email: datas.email
			}
		});

		if (user)
			return new User(user);

		const userCreated = await this.create(datas);

		return new User(userCreated);
	}

	async addRole(id: string, datas: AddRoleDto) {
		const user = await this.entity.findUnique({
			where: { id },
			include: {
				roles: true
			}
		});

		if (!user)
			throw new NotFoundException('Utilisateur introuvable');

		if (!await this.prisma.role.findUnique({ where: { id: datas.roleId } }))
			throw new NotFoundException('Rôle introuvable');

		user.roles.some(role => {
			if (role.id === datas.roleId)
				throw new ConflictException('Rôle déjà attribué');
		});

		return await this.entity.update({
			where: { id },
			data: {
				roles: {
					connect: {
						id: datas.roleId
					}
				}
			}
		});
	}

	async removeRole(id: string, datas: RemoveRoleDto) {
		const user = await this.entity.findUnique({
			where: { id },
			include: {
				roles: true
			}
		});

		if (!user)
			throw new NotFoundException('Utilisateur introuvable');

		if (!await this.prisma.role.findUnique({ where: { id: datas.roleId } }))
			throw new NotFoundException('Rôle introuvable');

		const contains = user.roles.some(role => role.id === datas.roleId);
		if (!contains)
			throw new NotFoundException(`L'utilisateur n'a pas le rôle`);

		return await this.entity.update({
			where: { id },
			data: {
				roles: {
					disconnect: {
						id: datas.roleId
					}
				}
			}
		});
	}

	async update(id: string, updateUserDto: UpdateUserDto) {
		if (!await this.entity.findUnique({ where: { id } }))
			throw new NotFoundException('Ressource inexistant');

		return await this.entity.update({
			where: { id },
			data: updateUserDto
		});
	}

	async remove(id: string) {
		if (!await this.entity.findUnique({ where: { id } }))
			throw new NotFoundException('Ressource inexistant');

		await this.prisma.role.deleteMany({
			where: { name: id }
		});

		return await this.entity.delete({
			where: { id }
		});
	}
}
