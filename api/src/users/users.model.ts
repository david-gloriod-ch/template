import { Prisma, User as PrismaUser } from "@prisma/client";
import { Model } from "src/generics/models";
import * as CryptoJS from 'crypto-js';
import * as argon2 from 'argon2';
import * as AuthMail from 'src/auth/auth.mail';
import { BadRequestException } from "@nestjs/common";
import { SendMailDto } from "src/mail/dtos/send-mail.dto";
import { MailService } from "src/mail/mail.service";

export class User extends Model<Prisma.UserDelegate> implements PrismaUser {
	private _mailService: MailService;
	email: string;
	stripeCustomerId: string;

	constructor(user: PrismaUser) {
		super(user.id, user.createdAt, user.updatedAt);
		this.entity = this.prisma.user;
		this.email = user.email;
	}

	set mailService(mailService: MailService) {
		this._mailService = mailService;
	}

	get mailService() {
		return this._mailService;
	}

	async notify(datas: SendMailDto) {
		await this.mailService.sendMail(datas);
	}

	toJSON() {
		return {
			...super.toJSON(),
			email: this.email,
			stripeCustomerId: this.stripeCustomerId,
		}
	}
}