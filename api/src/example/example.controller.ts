import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { ExampleService } from './example.service';
import { CreateExampleDto } from './dto/create-example.dto';
import { UpdateExampleDto } from './dto/update-example.dto';
import { IdPipe } from 'src/generics/pipes/id.pipe';
import { Permission } from 'src/permissions/decorators/permission.decorator';

@Controller('examples')
export class ExampleController {
	constructor(
		private readonly exampleService: ExampleService
	) {}

	@Post()
	create(@Body() datas: CreateExampleDto) {
		return this.exampleService.create(datas);
	}

	@Permission('example:read')
	@Get('/all')
	findAll() {
		return this.exampleService.findAll();
	}

	@Get()
	findPersonnal()
	{
		return this.exampleService.findPersonnal();
	}

	@Get(':id')
	findOne(@Param('id', IdPipe) id: string) {
		return this.exampleService.findOne(id);
	}

	@Patch(':id')
	update(@Param('id', IdPipe) id: string, @Body() datas: UpdateExampleDto) {
		return this.exampleService.update(id, datas);
	}

	@Delete(':id')
	remove(@Param('id', IdPipe) id: string) {
		return this.exampleService.remove(id);
	}
}
