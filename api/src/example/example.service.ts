import { Inject, Injectable } from '@nestjs/common';
import { CreateExampleDto } from './dto/create-example.dto';
import { UpdateExampleDto } from './dto/update-example.dto';
import { BaseService } from 'src/generics/service/base.service';
import { Prisma } from '@prisma/client';
import { REQUEST } from '@nestjs/core';
import { Request } from 'express';

@Injectable()
export class ExampleService extends BaseService<Prisma.ExampleDelegate> {

	constructor(
		@Inject(REQUEST)
		protected readonly request: Request,
	) {
		super(request);
		this.entity = this.prisma.example;
	}

	create(datas: CreateExampleDto) {
		this.entity.create({
			data: {
				...datas,
				userId: this.user.id
			}
		});
	}

	findAll() {
		return this.entity.findMany();
	}

	findPersonnal() {
		return this.entity.findMany({
			where: {
				userId: this.user.id
			}
		});
	}

	findOne(id: string) {
		return this.entity.findUnique({
			where: {
				id: id
			}
		});
	}

	update(id: string, datas: UpdateExampleDto) {
		return this.entity.update({
			where: {
				id: id
			},
			data: datas
		});
	}

	remove(id: string) {
		return this.entity.delete({
			where: {
				id: id
			}
		});
	}
}
