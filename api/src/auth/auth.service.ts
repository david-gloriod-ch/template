import { ConflictException, Inject, Injectable, NotFoundException, Redirect, UnauthorizedException } from '@nestjs/common';
import { SendPasswordDto } from './dto/send-password.dto';
import { PrismaService } from 'src/prisma.service';
import { BaseService } from 'src/generics/service/base.service';
import { Prisma } from '@prisma/client';
import { UsersService } from 'src/users/users.service';
import { SessionsService } from 'src/sessions/sessions.service';
import { CookieService } from 'src/core/services/cookie.service';
import * as argon2 from 'argon2';
import { Request, Response } from 'express';
import { ValidatePasswordDto } from './dto/validate-password.dto';
import * as jwt from 'jsonwebtoken';
import * as CryptoJS from 'crypto-js';
import { MailService } from 'src/mail/mail.service';
import { REQUEST } from '@nestjs/core';
import { Cookie } from 'src/core/classes/cookie.class';
import * as requestIp from 'request-ip';
import * as useragent from 'useragent';
import { UserInfosService } from 'src/core/services/user-infos.service';
import * as AuthMail from './auth.mail';
import { Session } from 'src/sessions/sessions.model';
import { User } from 'src/users/users.model';
import { stripe } from 'src/stripe/stripe';
import { aesEncrypt } from 'src/security/aes';

@Injectable()
export class AuthService extends BaseService<Prisma.SessionDelegate> {

	constructor(
		@Inject(REQUEST)
		protected readonly request: Request,
		private readonly usersService: UsersService,
		private readonly sessionsService: SessionsService,
		private readonly cookieService: CookieService,
		private readonly mailService: MailService,
		private readonly userInfosService: UserInfosService,
	)
	{
		super(request);
		this.entity = this.prisma.session;
	}

	async sendPassword(request: Request, datas: SendPasswordDto) {
		const sessionId = this.cookieService.getCookie('session', request.headers.cookie);
		const session = new Session(await this.sessionsService.entity.findFirst({where: {id: sessionId}}));
		session.setCredentials(datas.email);
		await session.saveCredentials();

		if (process.env.APP_ENV === 'dev')
			console.log(session.getPassword());
		else
			this.mailService.sendMail(session.getMailSendPassword({
				ip: requestIp.getClientIp(request),
				navigator: useragent.parse(request.headers['user-agent']).toString(),
				platform: useragent.parse(request.headers['user-agent']).os.toString(),
				device: useragent.parse(request.headers['user-agent']).device.toString()
			}));

		return {
			message: 'Un email vous a été envoyé',
			status: 200,
		};
	}

	async validatePassword(request: Request, datas: ValidatePasswordDto) {
		const sessionId = await this.cookieService.getCookie('session', request.headers.cookie);
		const session = new Session(await this.sessionsService.entity.findFirst({where: {id: sessionId}}));

		if (!await session.verifyCredentials(datas.email, datas.password))
		{
			await this.sessionsService.entity.update({ where: { id: sessionId }, data: { attempt: { increment: 1 }}});
			if (session.attempt === 5)
				await this.sessionsService.entity.update({ where: { id: sessionId }, data: { locked: true }});
			throw new UnauthorizedException('Adresse email ou mot de passe incorrect');
		}

		const user = await this.usersService.findFirstOrCreate({email: datas.email});
		user.mailService = this.mailService;
		await session.setUser(user.id);

		this.request['user_infos'] = {
			user: user,
			session: session.toJSON(),
			sessionId: session.getHashedId()
		};

		if (process.env.APP_ENV !== 'dev')
		{
			user.notify(session.getMailSuccessConnection({
				ip: requestIp.getClientIp(request),
				navigator: useragent.parse(request.headers['user-agent']).toString(),
				platform: useragent.parse(request.headers['user-agent']).os.toString(),
				device: useragent.parse(request.headers['user-agent']).device.toString()
			}));
		}

		const roles = await this.prisma.user.findUnique({
			where: {
				id: user.id,
			},
			include: {
				roles: {
					select: {
						id: true,
						title: true,
						permissions: {
							select: {
								permission: {
									select: {
										title: true
									}
								},
								level: true
							},
						}
					},
				}
			},
		});

		const customer = await this.prisma.stripeCustomer.findFirst({
			where: {
				userId: user.id
			},
			select: {
				id: true,
				customerId: true
			}
		});

		return jwt.sign({
			datas: {
				id: user.id,
				email: session.email,
				sessionId: CryptoJS.HmacSHA384(sessionId, process.env.SALT_SESSION_ID).toString(),
				roles: roles.roles.reduce((obj, role) => {
					obj[role.title] = role.id;
					return obj;
					}, {}
				),
				permissions: {
					...roles.roles.reduce((obj, role) => {
						if (role.permissions.length)
						{
							role.permissions.reduce((_, permission) => {
								const currentLevel = parseInt(obj[permission.permission.title], 2);
								const level = parseInt(`${permission.level}`, 2);
								const result = currentLevel | level;
								const string_result = result.toString(2).padStart(4, '0');
								if (string_result !== '0000')
									obj[permission.permission.title] = string_result;
								return obj;
							}, {})
						}
						return obj;
					}, {})
				},
				stripe: {
					id: customer ? customer.id : null,
					customerId: customer ? customer.customerId : null
				}
			}
		}, process.env.JWT_PRIVATE_KEY, {
			expiresIn: '10min',
			algorithm: 'ES512',
		});
	}

	async logout(response: Response) {
		const sessionId = this.cookieService.getCookie('session', this.request.headers.cookie);
		const session = await this.sessionsService.entity.findFirst({
			where: {id: sessionId},
			include: {
				user: true
			}
		});

		if (session)
			await this.sessionsService.entity.delete({ where: { id: sessionId } });

		const cookies = this.cookieService.getCookies(this.request.headers.cookie);
		const authorization = new Cookie('authorization', '');
		authorization.disable(['HttpOnly']);
		authorization.setOption('Max-Age', -1);
		response.setHeader('Set-Cookie', authorization.toString());
		response.send({ message: 'Déconnexion réussie' });
	}
}
