export const USER_INFOS = {
	user: {
		id: process.env.DEFAULT_USER_ID,
		email: process.env.DEFAULT_USER_EMAIL,
	},
	session: null,
	roles: [],
	permissions: {},
	stripe: {
		customerId: null
	},
	hashSessionId: null
}
