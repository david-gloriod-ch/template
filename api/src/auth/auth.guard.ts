import { CanActivate, ExecutionContext, Injectable, Optional, UnauthorizedException } from '@nestjs/common';
import { IS_PUBLIC_KEY } from './decorators/public.decorator';
import { Reflector } from '@nestjs/core';
import * as jwt from 'jsonwebtoken';
import { Request } from 'express';
import * as CryptoJS from 'crypto-js';

@Injectable()
export class AuthGuard implements CanActivate {
	constructor(
		private readonly reflector: Reflector,
	) {}

	async canActivate(_context: ExecutionContext): Promise<boolean> {
		const isPublic = this.reflector.get(IS_PUBLIC_KEY, _context.getHandler());
		if (isPublic)
			return true;

		const request: Request = _context.switchToHttp().getRequest();
		const user_infos = request['user_infos'];

		if ([undefined, process.env.DEFAULT_USER_ID].includes(user_infos.user?.id))
			throw new UnauthorizedException('Vous devez être connecté pour accéder à cette page');

		if (!user_infos.session)
			throw new UnauthorizedException('Votre session a expiré');

		if (!user_infos.session.verifyHash(user_infos.hashSessionId))
			throw new UnauthorizedException('Votre session est invalide');

		return true;
	}
}
