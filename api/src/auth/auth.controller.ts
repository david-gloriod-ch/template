import { Controller, Get, Post, Body, Patch, Param, Delete, Res, Req } from '@nestjs/common';
import { AuthService } from './auth.service';
import { SendPasswordDto } from './dto/send-password.dto';
import { Request, Response } from 'express';
import { ValidatePasswordDto } from './dto/validate-password.dto';
import { CookieService } from 'src/core/services/cookie.service';
import { Cookie } from 'src/core/classes/cookie.class';
import { Public } from './decorators/public.decorator';

@Controller('auth')
export class AuthController {
	constructor(
		private readonly service: AuthService,
	) {}

	@Public()
	@Post('/send-password')
	sendPassword(
		@Req() request: Request,
		@Body() datas: SendPasswordDto
	) {
		return this.service.sendPassword(request, datas);
	}

	@Public()
	@Post('/validate-password')
	async validatePassword(
		@Req() request: Request,
		@Res() response: Response,
		@Body() datas: ValidatePasswordDto
	) {
		const token = await this.service.validatePassword(request, datas);
		const cookie = new Cookie('authorization', token);
		cookie.disable(['HttpOnly']);
		response.setHeader('Set-Cookie', cookie.toString());
		response.status(200).send({
			message: 'Connexion réussie',
			authorization: token
		});
	}

	@Public()
	@Post('/logout')
	async logout(
		@Res() response: Response
	)
	{
		this.service.logout(response);
	}
}
