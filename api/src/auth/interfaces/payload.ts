export interface IJwtPayload {
	id: string;
	datas: IDatasPayload;
	iat?: number;
	exp?: number;
}

export interface IDatasPayload {
	id: string;
	email: string;
	sessionId: string;
	roles: string[];
	permissions: {[name:string]: string};
	stripe: {
		id: string;
		customerId: string;
	}
}