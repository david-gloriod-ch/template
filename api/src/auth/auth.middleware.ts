import { Injectable, NestMiddleware, UnauthorizedException } from '@nestjs/common';
import { Request, Response } from 'express';
import { CookieService } from 'src/core/services/cookie.service';
import { Cookie } from 'src/core/classes/cookie.class';
import * as jwt from 'jsonwebtoken';
import * as CryptoJS from 'crypto-js';
import { SessionsService } from 'src/sessions/sessions.service';
import { UsersService } from 'src/users/users.service';
import { IDatasPayload, IJwtPayload } from './interfaces/payload';
import { USER_INFOS } from './auth.config';
import { aesDecrypt } from 'src/security/aes';
import { Session } from 'src/sessions/sessions.model';

@Injectable()
export class AuthMiddleware implements NestMiddleware {

	constructor(
		private readonly cookieService: CookieService,
		private readonly sessionService: SessionsService,
		private readonly userService: UsersService,
	) {
	}

	async use(req: Request, res: Response, next: () => void) {
		const sessionId = this.cookieService.getCookie('session', req.headers.cookie);
		const authorization = this.cookieService.getCookie('authorization', req.headers.cookie);

		const user_infos = USER_INFOS;

		try {
			const verified = jwt.verify(authorization, process.env.JWT_PUBLIC_KEY, {
				ignoreExpiration: true
			});

			const datas: IDatasPayload = (verified as IJwtPayload).datas;

			user_infos.user = await this.userService.entity.findFirst({where: {id: datas.id}});
			user_infos.session = new Session(await this.sessionService.entity.findFirst({where: {id: sessionId, userId: datas.id}}));
			user_infos.roles = datas.roles;
			user_infos.permissions = datas.permissions;
			user_infos.hashSessionId = datas.sessionId;
			user_infos.stripe = datas.stripe;
			
		} catch (error) {}

		user_infos.session = new Session(await this.sessionService.entity.findUnique({where: {id: sessionId}}))
		req['user_infos'] = user_infos;
		next();
	}
}
