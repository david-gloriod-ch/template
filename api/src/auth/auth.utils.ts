export function generatePassword(length = 8) {
	return new Array(length).fill(null).map(() => Math.floor(Math.random() * 10)).join('');
}

export function generateBinaryKey(length = 128)
{
	return new Array(length).fill(null).map(() => String.fromCharCode(Math.floor(Math.random() * 10000))).join('');
}