import { PartialType } from '@nestjs/mapped-types';
import { SendPasswordDto } from './send-password.dto';
import { IsNotEmpty, IsString, Length } from 'class-validator';

export class ValidatePasswordDto extends SendPasswordDto {
	@IsString({
		message: 'Mauvais format de mot de passe',
	})
	@Length(8, 8, {
		message: 'Longueur de mot de passe incorrecte',
	})
	@IsNotEmpty({
		message: 'Mot de passe nécessaire',
	})
	password: string;
}
