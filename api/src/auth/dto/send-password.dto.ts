import { IsEmail, IsNotEmpty } from "class-validator";

export class SendPasswordDto {
	@IsEmail({
		allow_display_name: false,
		allow_utf8_local_part: true,
	}, { message: 'Email incorrect' })
	@IsNotEmpty({ message: 'Email nécessaire' })
	email: string;
}
