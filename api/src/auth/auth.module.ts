import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { PrismaService } from 'src/prisma.service';
import { UsersModule } from 'src/users/users.module';
import { SessionsModule } from 'src/sessions/sessions.module';
import { CookieService } from 'src/core/services/cookie.service';
import { MailModule } from 'src/mail/mail.module';
import { CoreModule } from 'src/core/core.module';

@Module({
	imports: [
		UsersModule,
		SessionsModule,
		MailModule,
		CoreModule,
	],
	controllers: [AuthController],
	providers: [
		AuthService,
		PrismaService,
		CookieService
	],
})
export class AuthModule {}
