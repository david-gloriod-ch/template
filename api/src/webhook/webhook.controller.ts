import { Controller, Get, Post, Body, Patch, Param, Delete, Req, Res } from '@nestjs/common';
import { WebhookService } from './webhook.service';
import { IdPipe } from 'src/generics/pipes/id.pipe';
import { Permission } from 'src/permissions/decorators/permission.decorator';
import { Public } from 'src/auth/decorators/public.decorator';
import { Request, Response } from 'express';

@Controller('webhook-manager')
export class WebhookController {
	constructor(
		private readonly webhookService: WebhookService
	) {}

	@Public()
	@Post()
	async webhook(@Req() request: Request, @Res() response: Response) {
		await this.webhookService.webhook(request, response);
	}
}
