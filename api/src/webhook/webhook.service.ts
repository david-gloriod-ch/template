import { BadRequestException, Inject, Injectable } from '@nestjs/common';
import { BaseService } from 'src/generics/service/base.service';
import { Prisma } from '@prisma/client';
import { REQUEST } from '@nestjs/core';
import { Request, Response } from 'express';
import Stripe from 'stripe';
import { stripe } from 'src/stripe/stripe';
import { StripePaymentWebhook } from 'src/stripe/payments/webhook';

@Injectable()
export class WebhookService {

	constructor(
		@Inject(REQUEST)
		protected readonly request: Request,
		private readonly stripePaymentWebhook: StripePaymentWebhook,
	) {
		
	}

	async webhook(request: Request, response: Response) {
		const body = request['rawBody'];
		const signature =this.request.headers['stripe-signature'] as string;

		let event: Stripe.Event;
		try {
			event = stripe.webhooks.constructEvent(
				body,
				signature,
				process.env.STRIPE_WEBHOOK_SECRET
			);
		} catch (error: unknown) {
			throw new BadRequestException("⚠️ Webhook Stripe Payment Error");
		}

		const session = event.data.object as Stripe.Checkout.Session;
		
		switch (session.mode) {
			case 'payment': {
				await this.stripePaymentWebhook.webhook(request, response);
			}
			break;
			default:
				break;
		}

		response.status(200).json({ received: true });
	}
}
