import { IsNotEmpty, IsString, IsUUID } from "class-validator";
import { AddPermissionDto } from "./add-permission.dto";

export class RemovePermissionDto extends AddPermissionDto {}