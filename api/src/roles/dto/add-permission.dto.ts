import { IsNotEmpty, IsString, IsUUID } from "class-validator";

export class AddPermissionDto {
	@IsUUID('4', { message: 'Permission invalide' })
	@IsNotEmpty({
		message: 'Permission nécessaire'
	})
	readonly permissionId: string;

	@IsString({
		message: 'Niveau invalide'
	})
	@IsNotEmpty({
		message: 'Niveau nécessaire'
	})
	readonly level: string;
}