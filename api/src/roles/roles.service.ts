import { ConflictException, Inject, Injectable, NotFoundException } from '@nestjs/common';
import { CreateRoleDto } from './dto/create-role.dto';
import { UpdateRoleDto } from './dto/update-role.dto';
import { BaseService } from 'src/generics/service/base.service';
import { Prisma } from '@prisma/client';
import { REQUEST } from '@nestjs/core';
import { Request } from 'express';
import { AddPermissionDto } from './dto/add-permission.dto';
import { RemovePermissionDto } from './dto/remove-permission.dto';
import { PAGINATION_CONFIG } from 'src/pagination/pagination.config';

@Injectable()
export class RolesService extends BaseService<Prisma.RoleDelegate> {

	constructor(
		@Inject(REQUEST)
		protected readonly request: Request
	) {
		super(request);
		this.entity = this.prisma.role;
	}

	async create(datas: CreateRoleDto) {
		return await this.entity.create({ data: datas });
	}

	async findAll(page: number) {
		const roles  = await this.entity.findMany({
			where: {
				unique_user: false
			},
			take: PAGINATION_CONFIG.take,
			skip: PAGINATION_CONFIG.skip(page),
			orderBy: { name: 'asc' }
		});
		const total = await this.entity.count();

		return {
			roles,
			total
		};
	}

	async fetchPermissions(page: number) {
		const roles  = await this.entity.findMany({
			where: {
				unique_user: false
			},
			include: {
				permissions: true
			},
			take: PAGINATION_CONFIG.take,
			skip: PAGINATION_CONFIG.skip(page),
			orderBy: { name: 'asc' }
		});
		const total = await this.entity.count();

		return {
			roles,
			total
		};
	}

	async findRolesUsers(page: number) {
		const roles  = await this.entity.findMany({
			where: {
				unique_user: true
			},
			take: PAGINATION_CONFIG.take,
			skip: PAGINATION_CONFIG.skip(page),
			orderBy: { name: 'asc' },
			include: {
				users: true,
				permissions: true
			}
		});
		const total = await this.entity.count();

		return {
			roles: roles.map(role => {
				const user = role.users[0];
				delete role.users;
				return {
					...role,
					user
				};
			}),
			total
		};
	}

	async findOne(id: string) {
		const role = await this.entity.findUnique({
			where: { id },
		});

		if (!role)
			throw new NotFoundException('Role introuvable');
		return role;
	}
	
	async update(id: string, datas: UpdateRoleDto) {
		return await this.entity.update({ where: { id }, data: datas });
	}

	async findPermissions(id: string) {
		const role = await this.entity.findUnique({
			where: { id },
			select: {
				permissions: true
			}
		});

		if (!role)
			throw new NotFoundException('Role introuvable');

		return role.permissions;
	}

	async addPermissions(id: string, datas: AddPermissionDto) {
		const role_permission = await this.prisma.rolePermissionLevel.findUnique({
			where: {
				roleId_permissionId: {
					roleId: id,
					permissionId: datas.permissionId
				}
			}
		});

		if (role_permission && parseInt(`${role_permission.level}`, 2) & parseInt(datas.level, 2))
			throw new ConflictException('Permission déjà ajoutée');

		const level = (role_permission ? parseInt(`${role_permission.level}`, 2) | parseInt(datas.level, 2) : datas.level).toString(2).padStart(4, '0');
		return await this.save(id, datas.permissionId, parseInt(level));
	}

	async removePermissions(id: string, datas: RemovePermissionDto) {
		const role_permission = await this.prisma.rolePermissionLevel.findUnique({
			where: {
				roleId_permissionId: {
					roleId: id,
					permissionId: datas.permissionId
				}
			}
		});

		if (!role_permission || !(parseInt(`${role_permission.level}`, 2) & parseInt(datas.level, 2)))
			throw new NotFoundException('Permission introuvable');

		const level = (role_permission ? parseInt(`${role_permission.level}`, 2) - parseInt(datas.level, 2) : datas.level).toString(2)
		return await this.save(id, datas.permissionId, parseInt(level));
	}

	async save(roleId: string, permissionId: string, level: number) {
		return await this.prisma.rolePermissionLevel.upsert({
			where: {
				roleId_permissionId: {
					roleId: roleId,
					permissionId: permissionId
				}
			},
			update: {
				level
			},
			create: {
				roleId: roleId,
				permissionId: permissionId,
				level
			}
		});
	}

	async remove(id: string) {
		return await this.entity.delete({ where: { id } });
	}
}
