import { Controller, Get, Post, Body, Patch, Param, Delete, Query } from '@nestjs/common';
import { RolesService } from './roles.service';
import { CreateRoleDto } from './dto/create-role.dto';
import { UpdateRoleDto } from './dto/update-role.dto';
import { PagePipe } from 'src/generics/pipes/page.pipe';
import { AddPermissionDto } from './dto/add-permission.dto';
import { RemovePermissionDto } from './dto/remove-permission.dto';
import { Permission } from 'src/permissions/decorators/permission.decorator';

@Controller('roles')
export class RolesController {
	constructor(private readonly service: RolesService) {}
	
	@Permission('role:create')
	@Post()
	create(@Body() datas: CreateRoleDto) {
		return this.service.create(datas);
	}

	@Permission('role:read')
	@Get()
	findAll(@Query('page', PagePipe) page: number) {
		return this.service.findAll(page);
	}

	@Permission('role:read')
	@Get('permissions')
	fetchPermissions(@Query('page', PagePipe) page: number) {
		return this.service.fetchPermissions(page);
	}

	@Permission('role:read')
	@Get('roles-users')
	findRolesUsers(@Query('page', PagePipe) page: number) {
		return this.service.findRolesUsers(page);
	}

	@Permission('role:read')
	@Get(':id/permissions')
	findPermissions(@Param('id') id: string) {
		return this.service.findPermissions(id);
	}

	@Permission('role-permission:create')
	@Post(':id/permissions')
	addPermissions(@Param('id') id: string, @Body() datas: AddPermissionDto) {
		return this.service.addPermissions(id, datas);
	}

	@Permission('role-permission:delete')
	@Delete(':id/permissions')
	removePermissions(@Param('id') id: string, @Body() datas: RemovePermissionDto) {
		return this.service.removePermissions(id, datas);
	}

	@Permission('role:read')
	@Get(':id')
	findOne(@Param('id') id: string) {
		return this.service.findOne(id);
	}

	@Permission('role:update')
	@Patch(':id')
	update(@Param('id') id: string, @Body() datas: UpdateRoleDto) {
		return this.service.update(id, datas);
	}

	@Permission('role:delete')
	@Delete(':id')
	remove(@Param('id') id: string) {
		return this.service.remove(id);
	}
}
