import * as CryptoJS from 'crypto-js';

export function aesEncrypt(text: string, password?: string) {
	password = CryptoJS.SHA384(password || process.env.APP_KEY).toString();
	return CryptoJS.AES.encrypt(text, password).toString();
}

export function aesDecrypt(encrypted: string, password?: string) {
	password = CryptoJS.SHA384(password || process.env.APP_KEY).toString();
	const bytes = CryptoJS.AES.decrypt(encrypted, password);

	return bytes.toString(CryptoJS.enc.Utf8);
}