import { ForbiddenException, Injectable, NestMiddleware } from '@nestjs/common';
import { Request, Response } from 'express';
import { CookieService } from 'src/core/services/cookie.service';
import * as CryptoJS from 'crypto-js';

@Injectable()
export class CSRFMiddleware implements NestMiddleware {
	constructor(
		private readonly cookieService: CookieService,
	) {}

	use(req: Request, res: Response, next: () => void) {
		if (['post', 'patch', 'put'].includes(req.method.toLowerCase()))
		{
			const sessionId = this.cookieService.getCookie('session', req.headers.cookie);

			const csrf = {
				cookie: this.cookieService.getCookie('csrf', req.headers.cookie),
				session: CryptoJS.HmacSHA384(sessionId, process.env.APP_KEY).toString()
			};

			if (csrf.cookie !== csrf.session)
				throw new ForbiddenException('Token non valide');
		}
		
		next();
	}
}
