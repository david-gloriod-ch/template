import { Body, Controller, Get, Inject, Post, Query, Req, Res } from '@nestjs/common';
// import { StripeSubscriptionService } from './subscription/services/subscription.service';
import { Public } from 'src/auth/decorators/public.decorator';
import { Request, Response } from 'express';
import { StripePaymentService } from './payments/service';
import { StripePaymentWebhook } from './payments/webhook';

@Controller('stripe')
export class StripeController {
	constructor(
		// private readonly stripeSubscriptionService: StripeSubscriptionService,
		private readonly stripePaymentService: StripePaymentService,
		private readonly stripePaymentWebhook: StripePaymentWebhook
	) {}

	// @Get('start-payment')
	// async startPayment() {
	// 	return await this.stripePaymentService.startPayment('12345');
	// }

	// @Get('subscription')
	// async getDataStripeUser() {
	// 	return await this.stripeSubscriptionService.getDataStripeUser();
	// }

	// @Get('create-payment')
	// async createPayment() {
	// 	return await this.stripePaymentService.createPayment();
	// }

	// @Get('customer-portal')
	// async createCustomerPortal() {
	// 	return await this.stripeSubscriptionService.createCustomerPortal();
	// }

	@Public()
	@Post('/payment/webhook')
	async swebhook(@Req() request: Request, @Res() response: Response) {
		await this.stripePaymentWebhook.webhook(request, response);
	}
}
