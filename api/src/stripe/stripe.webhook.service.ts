import { BadRequestException, Inject, Injectable, NotFoundException } from '@nestjs/common';
import { REQUEST } from '@nestjs/core';
import { Request, Response, response } from 'express';
import { BaseService } from 'src/generics/service/base.service';
import { Prisma } from '@prisma/client';
import Stripe from 'stripe';
import { stripe } from './stripe';

@Injectable()
export class StripeWebhook<T> extends BaseService<T> {

	constructor(
		@Inject(REQUEST)
		protected readonly request: Request,
	) {
		super(request);
	}

	verifySignature(request: Request) {
		const body = request['rawBody'];
		const signature = this.request.headers['stripe-signature'] as string;

		let event: Stripe.Event;
		try {
			event = stripe.webhooks.constructEvent(
				body,
				signature,
				process.env.STRIPE_WEBHOOK_SECRET
			);
		} catch (error: unknown) {
			throw new BadRequestException("⚠️ Webhook Stripe Payment Error");
		}

		return event;
	}
}
