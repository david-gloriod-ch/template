import { BadRequestException, Injectable, NestMiddleware } from '@nestjs/common';
import { Request, Response } from 'express';
import * as bodyParser from 'body-parser';

@Injectable()
export class StripeMiddleware implements NestMiddleware {
	use(req: Request, res: Response, next: () => void) {
		if (!req.originalUrl.startsWith('/stripe'))
			return next();
		bodyParser.raw({ type: '*/*' })(req, res, (err: any) => {
			if (err) {
				throw new BadRequestException('Invalid body');
			}
			req['rawBody'] = req.body;
			next();
		});
	}
}
