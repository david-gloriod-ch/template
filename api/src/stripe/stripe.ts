import Stripe from 'stripe';

export const stripe = new Stripe(process.env.STRIPE_KEY_SECRET, {
	typescript: true,
});

export const getStripeSession = async ({priceId, domainUrl, customerId, mode, metadata}: {priceId: string, domainUrl: string, customerId: string, mode: 'payment' | 'setup' | 'subscription', metadata?: any}) => {
	const session = await stripe.checkout.sessions.create({
		customer: customerId,
		mode: mode,
		billing_address_collection: 'auto',
		line_items: [
			{
				price: priceId,
				quantity: 1,
			},
		],
		payment_method_types: ['card'],
		customer_update: {
			address: 'auto',
			name: 'auto',
		},
		metadata: metadata,
		success_url: `${domainUrl}/stripe/payment/success`,
		cancel_url: `${domainUrl}/stripe/payment/cancel`,
	});
	return session;
}