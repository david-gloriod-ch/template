import { Module, forwardRef } from '@nestjs/common';
// import { StripeSubscriptionService } from './subscription/services/subscription.service';
import { StripeController } from './stripe.controller';
import { StripePaymentService } from './payments/service';
import { StripePaymentWebhook } from './payments/webhook';
import { PermissionsModule } from 'src/permissions/permissions.module';
import { UsersModule } from 'src/users/users.module';
import { MailModule } from 'src/mail/mail.module';
import { StripeCustomerService } from './customers/service';
import { StripeWebhook } from './stripe.webhook.service';

@Module({
	imports: [
		PermissionsModule,
		forwardRef(() => UsersModule),
		MailModule,
	],
	controllers: [StripeController],
	providers: [
		// StripeSubscriptionService,
		StripePaymentService,
		StripeCustomerService,
		StripePaymentWebhook,
		StripeWebhook
	],
	exports: [
		// StripeSubscriptionService,
		StripePaymentService,
		StripeCustomerService,
		StripePaymentWebhook,
	],
})
export class StripeModule {}
