import { BadRequestException, Inject, Injectable, NotFoundException, UnauthorizedException } from '@nestjs/common';
import { getStripeSession, stripe } from '../stripe';
import { REQUEST } from '@nestjs/core';
import { Request, Response, response } from 'express';
import { BaseService } from 'src/generics/service/base.service';
import { Prisma } from '@prisma/client';
import Stripe from 'stripe';
import { PermissionsService } from 'src/permissions/permissions.service';
import { UsersService } from 'src/users/users.service';
import { MailService } from 'src/mail/mail.service';
import { StripeCustomerService } from '../customers/service';
import { PrismaService } from 'src/prisma.service';

@Injectable()
export class StripePaymentWebhook {
	constructor(
		@Inject(REQUEST)
		protected readonly request: Request,
		private readonly stripeCustomerService: StripeCustomerService,
		private readonly permissionsService: PermissionsService,
		private readonly usersService: UsersService,
		private readonly mailService: MailService
	) {}

	get prisma(): PrismaService {
		return PrismaService.prisma;
	}

	async webhook(req: Request, response: Response) {
		const body = req['rawBody'];
		const signature =this.request.headers['stripe-signature'] as string;
		
		let event: Stripe.Event;
		try {
			event = stripe.webhooks.constructEvent(
				body,
				signature,
				process.env.STRIPE_WEBHOOK_SECRET
			);
		} catch (error: unknown) {
			throw new UnauthorizedException("⚠️ Webhook Stripe Payment Error");
		}

		const session = event.data.object as Stripe.Checkout.Session;
		const stripeCustomer = await this.stripeCustomerService.getByCustomerId(session.customer as string);

		if (event.type === 'checkout.session.completed')
		{
			// const client = await stripe.checkout.sessions.listLineItems(session.id);
			// await this.prisma.stripeCustomerProducts.create({
			// 	data: {
			// 		productId: client.data[0].price.product as string,
			// 		customerId: session.metadata['customerId'],
			// 		priceId: client.data[0].price.id,
			// 		sessionId: session.metadata['sessionId'],
			// 		stripeSessionId: session.id
			// 	},
			// });
		}
	}
}
