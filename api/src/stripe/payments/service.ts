import { BadRequestException, Inject, Injectable, NotFoundException } from '@nestjs/common';
import { getStripeSession, stripe } from '../stripe';
import { REQUEST } from '@nestjs/core';
import { Request, Response, response } from 'express';
import { BaseService } from 'src/generics/service/base.service';
import { Prisma } from '@prisma/client';
import { IDatasPayload } from 'src/auth/interfaces/payload';
import { StripeCustomerService } from '../customers/service';

@Injectable()
export class StripePaymentService {
	constructor(
		@Inject(REQUEST)
		protected readonly request: Request,
		private readonly stripeCustomerService: StripeCustomerService,
	) {}

	get user(): IDatasPayload {
		return this.request['user_infos'].user;
	}

	async startPayment(
		data:
		{priceId: string, type: 'payment' | 'subscription', customerId: string, metadata?: any}
	) {
		const customer = await this.stripeCustomerService.getByUserId(this.user.id);
		const session_url = await getStripeSession({
			customerId: customer.id,
			domainUrl: process.env.APP_URL,
			priceId: data.priceId,
			mode: data.type,
			metadata: data.metadata,
		});

		return session_url;
	}

	// async createPayment() {
	// 	const customer = await this.prisma.user.findUnique({
	// 		where: {
	// 			id: this.user.id,
	// 		},
	// 	});

	// 	const subscriptionUrl = await getStripeSession({
	// 		customerId: customer.stripeCustomerId,
	// 		domainUrl: process.env.APP_URL,
	// 		priceId: process.env.STRIPE_PAYMENT_ID,
	// 		mode: 'payment',
	// 	});

	// 	return subscriptionUrl;
	// }

	// async createCustomerPortal() {
	// 	const customer = await this.prisma.user.findUnique({
	// 		where: {
	// 			id: this.user.id,
	// 		},
	// 	});

	// 	const portalUrl = await stripe.billingPortal.sessions.create({
	// 		customer: customer.stripeCustomerId,
	// 		return_url: `${process.env.APP_URL}/payment`,
	// 	});

	// 	return portalUrl;
	// }

	// async getPaymentUrl() {
	// 	const customer = await stripe.customers.create({
	// 		email: process.env.ADMINISTATOR_EMAIL,
	// 	});

	// 	const url = await getStripeSession({
	// 		priceId: process.env.STRIPE_PAYMENT_ID,
	// 		domainUrl: process.env.APP_URL,
	// 		customerId: customer.id,
	// 		mode: 'payment',
	// 	});

	// 	return url;
	// }
}
