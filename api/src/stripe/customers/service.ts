import { BadRequestException, Inject, Injectable, NotFoundException } from '@nestjs/common';
import { getStripeSession, stripe } from '../stripe';
import { REQUEST } from '@nestjs/core';
import { Request, Response } from 'express';
import { BaseService } from 'src/generics/service/base.service';
import { Prisma } from '@prisma/client';

@Injectable()
export class StripeCustomerService extends BaseService<Prisma.StripeCustomerDelegate> {
	constructor(
		@Inject(REQUEST)
		protected readonly request: Request
	) {
		super(request);
		this.entity = this.prisma.stripeCustomer;
	}

	async getByUserId(userId: string) {
		const customer = await this.entity.findFirst({
			where: {
				userId: userId,
			},
		});

		if (!customer)
			throw new NotFoundException('Customer introuvable');

		return await stripe.customers.retrieve(customer.customerId);
	}

	async getByCustomerId(customerId: string) {
		return await stripe.customers.retrieve(customerId);
	}

	async create(datas: Prisma.UserWhereUniqueInput) {
		const stripeCustomer = await stripe.customers.create({
			email: datas.email,
		});

		const customer = await this.entity.create({
			data: {
				userId: datas.id,
				customerId: stripeCustomer.id,
			},
		});

		return customer;
	}
}
