import { Injectable } from '@nestjs/common';
import { PrismaService } from './prisma.service';

@Injectable()
export class AppService {
	constructor(
		private readonly prisma: PrismaService
	) {
	}

	async init() {
		await this.initUsers();
		await this.initRoles();
		await this.initPermissions();
		await this.initAdministrators();
	}

	async initUsers() {
		const user = await this.prisma.user.findFirst({
			where: {
				id: process.env.DEFAULT_USER_ID
			}
		});

		if (!user)
		{
			await this.prisma.user.create({
				data: {
					id: process.env.DEFAULT_USER_ID,
					email: process.env.DEFAULT_USER_EMAIL,
				}
			});
			await this.prisma.userInfos.create({
				data: {
					userId: process.env.DEFAULT_USER_ID,
					number_of_sessions: 0
				}
			});
		}
	}

	async initPermissions() {
		const permission = await this.prisma.permission.findFirst({
			where: {
				id: process.env.ADMINISTRATOR_PERMISSION_ID
			}
		});

		if (!permission)
		{
			await this.prisma.permission.create({
				data: {
					id: process.env.ADMINISTRATOR_PERMISSION_ID,
					name: process.env.ADMINISTRATOR_PERMISSION_NAME,
					title: process.env.ADMINISTRATOR_PERMISSION_TITLE,
					description: process.env.ADMINISTRATOR_PERMISSION_DESCRIPTION
				}
			});
			await this.prisma.rolePermissionLevel.create({
				data: {
					roleId: process.env.ADMINISTRATOR_ROLE_ID,
					permissionId: process.env.ADMINISTRATOR_PERMISSION_ID,
					level: 1111
				}
			});
		}
	}

	async initRoles() {
		const role = await this.prisma.role.findFirst({
			where: {
				id: process.env.ADMINISTRATOR_ROLE_ID
			}
		});

		if (!role)
		{
			await this.prisma.role.createMany({
				data: [
					{
						id: process.env.ADMINISTRATOR_ROLE_ID,
						name: process.env.ADMINISTRATOR_ROLE_NAME,
						title: process.env.ADMINISTRATOR_ROLE_TITLE,
						description: process.env.ADMINISTRATOR_ROLE_DESCRIPTION
					},
					{
						id: process.env.ADMINISTRATOR_ROLE_USER_ID,
						name: process.env.ADMINISTRATOR_USER_ID,
						title: process.env.ADMINISTRATOR_USER_ID,
						description: `Rôle de l'utilisateur ${process.env.ADMINISTRATOR_USER_ID}`,
						unique_user: true
					}
				]
			});
		}
	}

	async initAdministrators() {
		const user = await this.prisma.user.findFirst({
			where: {
				id: process.env.ADMINISTRATOR_USER_ID
			}
		});

		if (!user)
		{
			await this.prisma.user.create({
				data: {
					id: process.env.ADMINISTRATOR_USER_ID,
					email: process.env.ADMINISTRATOR_USER_EMAIL,
					roles: {
						connect: [
							{
								id: process.env.ADMINISTRATOR_ROLE_ID
							},
							{
								id: process.env.ADMINISTRATOR_ROLE_USER_ID
							}
						],
					},
				},
			});
			await this.prisma.userInfos.create({
				data: {
					userId: process.env.ADMINISTRATOR_USER_ID,
					number_of_sessions: 0
				}
			});
		}
	}

	getHello(): string {
		return 'Hello World!';
	}
}
