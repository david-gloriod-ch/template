import { NestFactory, Reflector } from '@nestjs/core';
import { AppModule } from './app.module';
import { ValidationPipe } from '@nestjs/common';
import helmet from 'helmet';
import { validate } from 'uuid';
import { AuthGuard } from './auth/auth.guard';
import { SessionsService } from './sessions/sessions.service';
import { StripeMiddleware } from './stripe/stripe.middleware';
import { WebhookMiddleware } from './webhook/webhook.middleware';

async function bootstrap() {
	const app = await NestFactory.create(AppModule);
	app.useGlobalPipes(new ValidationPipe({
		transform: true,
		whitelist: true,
		forbidNonWhitelisted: true,
		forbidUnknownValues: true
	}));
	app.use(helmet());
	app.use(new StripeMiddleware().use);
	app.use(new WebhookMiddleware().use);
	app.enableCors({
		origin: `${process.env.APP_URL}`,
		credentials: true
	})
	await app.listen(process.env.SERVER_PORT);

}
bootstrap();
