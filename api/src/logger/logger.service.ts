import { Injectable } from '@nestjs/common';

@Injectable()
export class LoggerService {
	constructor() {
	}

	async log(message: string) {
		console.log(message);
	}
}
