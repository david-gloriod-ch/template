import { CallHandler, ExecutionContext, Injectable, NestMiddleware } from '@nestjs/common';
import { Prisma } from '@prisma/client';
import { Request, Response } from 'express';
import { Observable } from 'rxjs';
import { PrismaService } from 'src/prisma.service';
import * as requestIp from 'request-ip';
import * as useragent from 'useragent';

@Injectable()
export class LoggerMiddleware implements NestMiddleware {
	constructor(
		private readonly prisma: PrismaService,
	) {}

	async use(request: Request, response: Response, next: () => void) {
		await this.prisma.logger.create({
			data: {
				uri: request.originalUrl,
				method: request.method,
				status: response.statusCode,
				body: JSON.stringify(request.body),
				ip: requestIp.getClientIp(request),
				userAgent: request.headers['user-agent'],
				cookies: request.headers.cookie,
			}
		})

		next();
	}
}
