import { tCookie, DefaultCookieConfig } from "../types/cookies";

export class Cookie {
	private readonly name: string;
	private readonly value: string;
	private readonly options: {
		[key: string]: string | boolean | number,
	};

	constructor(name: string, value: string) {
		this.name = `${process.env.APP_NAME.replaceAll(' ', '-').toLowerCase()}-${name}`;
		this.value = value;
		this.options = {...DefaultCookieConfig};

		if (process.env.APP_ENV === 'dev') {
			this.disable(['Secure']);
		}
	}

	public disable(options: string[]): void {
		options.forEach(option => {
			delete this.options[option];
		});
	}

	public enable(options: string[]): void {
		options.forEach(option => {
			this.options[option] = DefaultCookieConfig[option];
		});
	}

	public setOption(option: string, value: string | number | boolean): void {
		this.options[option] = value;
	}

	private optionsToString(): string {
		let options = '';
		for (const key in this.options) {
			options += `${key}=${this.options[key]}; `;
		}
		return options;
	}

	toString() {
		return `${this.name}=${this.value}; ${this.optionsToString()}`;
	}
}
