import { Inject, Injectable, UnauthorizedException } from "@nestjs/common";
import { REQUEST } from "@nestjs/core";
import { Prisma } from "@prisma/client";
import { Request } from "express";
import { IDatasPayload } from "src/auth/interfaces/payload";
import { BaseService } from "src/generics/service/base.service";

@Injectable()
export class UserInfosService extends BaseService<Prisma.UserInfosDelegate> {

	constructor(
		@Inject(REQUEST)
		protected readonly request: Request,
	)
	{
		super(request);
		this.entity = this.prisma.userInfos;
	}

	async incrementSession() {
		const user = this.request['user_infos'].user;
		if (!user)
			return;
		await this.entity.update({
			where: {
				userId: user.id
			},
			data: {
				number_of_sessions: {
					increment: 1
				}
			}
		});
	}

	async decrementSession() {
		const user = this.request['user_infos'].user;
		if (!user)
			return;
		await this.entity.update({
			where: {
				userId: user.id
			},
			data: {
				number_of_sessions: {
					decrement: 1
				}
			}
		});
	}
}
