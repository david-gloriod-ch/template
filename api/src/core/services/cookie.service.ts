import { Injectable } from "@nestjs/common";
import { tCookie, DefaultCookieConfig } from "../types/cookies";

@Injectable()
export class CookieService {
	getCookie(name: string, cookie: string): string {
		name = `${process.env.APP_NAME.replaceAll(' ', '-').toLowerCase()}-${name}`;
		const value = `; ${cookie}`;
		const parts = value.split(`; ${name}=`);
		if (parts.length === 2) {
			return parts.pop().split(";").shift();
		}
	}

	getCookies(cookie: string): tCookie {
		const cookies: tCookie = {};
		const values = cookie.split(';');
		values.forEach((value) => {
			const parts = value.split('=');
			cookies[parts[0].trim()] = parts[1];
		});
		return cookies;
	}
}
