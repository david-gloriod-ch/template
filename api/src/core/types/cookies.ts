export type tCookie = {
	[key:string]: string | boolean | number,
}

export const DefaultCookieConfig: tCookie = {
	'Domain': process.env.COOKIE_DOMAIN,
	'Path': '/',
	'Secure': process.env.APP_ENV !== 'dev',
	'HttpOnly': true,
	'SameSite': 'Lax',
	'Max-Age': 3600 * 24 * 30
}
