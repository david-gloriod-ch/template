import { Global, Module } from '@nestjs/common';
import { CookieService } from './services/cookie.service';
import { UserInfosService } from './services/user-infos.service';

@Module({
	providers: [
		CookieService,
		UserInfosService
	],
	exports: [
		CookieService,
		UserInfosService
	],
})
export class CoreModule {}
