import { Prisma, Session as PrismaSession } from "@prisma/client";
import { Model } from "src/generics/models";
import * as CryptoJS from 'crypto-js';
import * as argon2 from 'argon2';
import * as AuthMail from 'src/auth/auth.mail';
import { BadRequestException } from "@nestjs/common";

export class Session extends Model<Prisma.SessionDelegate> implements PrismaSession {
	email: string;
	password: string;
	userId: string;
	address: string;
	navigator: string;
	device: string;
	platform: string;
	attempt: number;
	locked: boolean;

	constructor(session: PrismaSession) {
		super(session.id, session.createdAt, session.updatedAt);
		this.entity = this.prisma.session;
		this.email = session.email;
		this.password = session.password;
		this.userId = session.userId;
		this.address = session.address;
		this.navigator = session.navigator;
		this.device = session.device;
		this.platform = session.platform;
		this.attempt = session.attempt;
		this.locked = session.locked;
	}

	getPassword() {
		return this.password;
	}

	getHashedId() {
		return CryptoJS.HmacSHA384(this.id, process.env.SALT_SESSION_ID).toString();
	}

	verifyHash(hash: string) {
		return this.getHashedId() === hash;
	}

	setCredentials(email: string) {
		this.email = email;
		this.password = this.generatePassword();
	}

	async verifyCredentials(email: string, password: string) {
		return this.email === email && await this.verifyPassword(password);
	}

	async verifyPassword(password: string) {
		return !password || await argon2.verify(this.password, `${password}.${process.env.SALT_PASSWORD}`);
	}

	generatePassword(length = 8) {
		this.password = new Array(length).fill(null).map(() => Math.floor(Math.random() * 10)).join('');
		return this.password;
	}

	async saveCredentials() {
		if (!this.email || !this.password)
			throw new BadRequestException('Adresse mail et mot de passe requis');

		await this.entity.update({
			where: {
				id: this.id
			},
			data: {
				email: this.email,
				password: await argon2.hash(`${this.password}.${process.env.SALT_PASSWORD}`)
			}
		});
	}

	async removeCredentials() {
		await this.entity.update({
			where: {
				id: this.id
			},
			data: {
				email: null,
				password: null
			}
		});

		this.password = null;
	}

	async setUser(userId: string) {
		this.userId = userId;

		const promise = this.entity.update({
			where: {
				id: this.id
			},
			data: {
				user: {
					connect: {
						id: userId
					}
				}
			}
		});

		await Promise.all([this.removeCredentials(), promise]);
	}

	getMailSendPassword(requestInformations: {ip: string, navigator: string, platform: string, device: string}) {
		return {
			to: this.email,
			subject: `Votre mot de passe est ${this.password}`,
			text: AuthMail.sendPassword(
				this.password,
				requestInformations.ip,
				requestInformations.navigator,
				requestInformations.device,
				requestInformations.platform,
			)
		}
	}

	getMailSuccessConnection(requestInformations: {ip: string, navigator: string, platform: string, device: string}) {
		return {
			to: this.email,
			subject: `Nouvelle connexion à votre compte`,
			text: AuthMail.validatePassword(
				requestInformations.ip,
				requestInformations.navigator,
				requestInformations.platform,
				requestInformations.device
			)
		}
	}

	toJSON() {
		return {
			...super.toJSON(),
			email: this.email,
			userId: this.userId,
			address: this.address,
			navigator: this.navigator,
			device: this.device,
			platform: this.platform
		}
	}
}