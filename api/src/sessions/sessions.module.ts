import { Module } from '@nestjs/common';
import { SessionsService } from './sessions.service';
import { SessionsController } from './sessions.controller';
import { CoreModule } from 'src/core/core.module';

@Module({
	imports: [
		CoreModule,
	],
	controllers: [SessionsController],
	providers: [
		SessionsService,
	],
	exports: [
		SessionsService
	]
})
export class SessionsModule {}
