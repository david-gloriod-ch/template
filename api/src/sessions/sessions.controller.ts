import { Controller, Get, Post, Body, Patch, Param, Delete, Res, Query } from '@nestjs/common';
import { SessionsService } from './sessions.service';
import { Response } from 'express';
import { Public } from 'src/auth/decorators/public.decorator';
import { PagePipe } from 'src/generics/pipes/page.pipe';

@Controller('sessions')
export class SessionsController {
	constructor(
		private readonly service: SessionsService
	) {}

	@Public()
	@Get('create')
	async create() {
		return {
			message: 'Session created',
		};
	}

	@Get()
	async fetchAll(@Query('page', PagePipe) page: number) {
		return this.service.fetchAll(page);
	}

	@Delete('/:id')
	async delete(@Param('id') id: string) {
		return this.service.delete(id);
	}
}
