import { ConflictException, Inject, Injectable, NotFoundException } from '@nestjs/common';
import { PrismaService } from 'src/prisma.service';
import { BaseService } from 'src/generics/service/base.service';
import { Prisma } from '@prisma/client';
import * as CryptoJS from 'crypto-js';
import { REQUEST } from '@nestjs/core';
import { Request } from 'express';
import * as requestIp from 'request-ip';
import * as useragent from 'useragent';
import { PAGINATION_CONFIG } from 'src/pagination/pagination.config';

@Injectable()
export class SessionsService extends BaseService<Prisma.SessionDelegate> {

	constructor(
		@Inject(REQUEST)
		protected readonly request: Request
	)
	{
		super(request);
		this.entity = this.prisma.session;
	}

	async create() {
		const agent = useragent.parse(this.request.headers['user-agent']);
		return await this.entity.create({ data: {
			address: requestIp.getClientIp(this.request),
			navigator: agent.toString(),
			platform: agent.os.toString(),
			device: agent.device.toString(),
		} });
	}

	async fetchAll(page: number) {
		const not_current_session = await this.entity.findMany({ where
			: { user: { id: this.user.id }, id: { not: this.session.id } },
			take: PAGINATION_CONFIG.take, skip: PAGINATION_CONFIG.skip(page), orderBy: { createdAt: 'desc' }
		});
		const number_of_sessions = await this.prisma.userInfos.findFirst({ where: { user: { id: this.user.id } }});
		return {
			sessions: not_current_session,
			total: number_of_sessions.number_of_sessions
		};
	}

	async delete(id: string) {
		const where = { id, user: { id: this.user.id } };
		const session = await this.entity.findUnique({ where });
		if (!session) {
			throw new NotFoundException('Session introuvable');
		}
		return await this.entity.delete({ where });
	}
}
