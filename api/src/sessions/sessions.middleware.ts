import { ForbiddenException, Injectable, NestMiddleware, UnauthorizedException } from '@nestjs/common';
import { Request, Response } from 'express';
import { SessionsService } from './sessions.service';
import { CookieService } from 'src/core/services/cookie.service';
import { Cookie } from 'src/core/classes/cookie.class';
import * as CryptoJS from 'crypto-js';

@Injectable()
export class SessionsMiddleware implements NestMiddleware {
	constructor(
		private readonly service: SessionsService,
		private readonly cookieService: CookieService,
	) {}

	async createNewSession(service: SessionsService, response: Response) {
		const session = await service.create();
		const cookie = new Cookie('session', session.id);
		const csrfCookie = new Cookie('csrf', CryptoJS.HmacSHA384(session.id, process.env.APP_KEY).toString());

		csrfCookie.disable(['HttpOnly']);
		response.setHeader('Set-Cookie', [
			cookie.toString(),
			csrfCookie.toString()
		]);
		response.send({message: 'Session created'});
	}

	isIgnoringUrl(url: string) {
		return url.startsWith('/webhook-manager') || url.startsWith('/stripe');
	}

	async use(req: Request, res: Response, next: () => void) {
		const sessionId = this.cookieService.getCookie('session', req.headers.cookie);

		if (this.isIgnoringUrl(req.originalUrl))
			return next();

		if (!sessionId)
			return await this.createNewSession(this.service, res);
		else if (sessionId)
		{
			const session = await this.service.entity.findUnique({where: {id: sessionId}});
			if (!session || session.locked)
				return await this.createNewSession(this.service, res);
		}
		next();
	}
}
