import { Body, Controller, Delete, Get, Patch, Res } from '@nestjs/common';
import { ProfileService } from './profile.service';
import { UpdateEmailDto } from './dto/update-email.dto';
import { ValidateEmailDto } from './dto/validate-email.dto';
import { Response } from 'express';
import { Cookie } from 'src/core/classes/cookie.class';

@Controller('profile')
export class ProfileController {
	constructor(
		private readonly service: ProfileService
	) {}

	@Get()
	fetch() {
		return this.service.currentUser();
	}

	@Patch('/update-email')
	updateEmail(@Body() datas: UpdateEmailDto) {
		return this.service.updateEmail(datas);
	}

	@Patch('/validate-email')
	async validateEmail(
		@Body() datas: ValidateEmailDto,
		@Res() response: Response
	) {
		const token = await this.service.validateEmail(datas);
		const cookie = new Cookie('authorization', token);
		cookie.disable(['HttpOnly']);
		response.setHeader('Set-Cookie', cookie.toString());
		response.status(200).send({
			message: 'Email validé',
			authorization: token
		});
	}

	@Delete()
	async delete() {
		return this.service.delete();
	}
}
