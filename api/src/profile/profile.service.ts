import { BadRequestException, ConflictException, ForbiddenException, Inject, Injectable, NotFoundException, UnauthorizedException } from '@nestjs/common';
import { BaseService } from 'src/generics/service/base.service';
import { Prisma } from '@prisma/client';
import { REQUEST } from '@nestjs/core';
import { Request } from 'express';
import { generatePassword } from 'src/auth/auth.utils';
import { UpdateEmailDto } from './dto/update-email.dto';
import { ValidateEmailDto } from './dto/validate-email.dto';
import * as jwt from 'jsonwebtoken';
import * as ProfileMail from './profile.mail';
import { MailService } from 'src/mail/mail.service';
import * as requestIp from 'request-ip';
import * as useragent from 'useragent';

@Injectable()
export class ProfileService extends BaseService<Prisma.UserDelegate> {

	private _promises: Promise<any>[] = [];

	constructor(
		@Inject(REQUEST)
		protected readonly request: Request,
		private readonly mailService: MailService
	)
	{
		super(request);
		this.entity = this.prisma.user;
	}

	currentUser() {
		return this.request['user_infos'].user;
	}

	private async updating() {
		return await this.prisma.emailUpdating.findFirst({
			where: {
				userId: this.request['user_infos'].user.id,
			}
		})
	}

	async updateEmail(datas: UpdateEmailDto) {
		if (await this.entity.findFirst({ where: { email: datas.email } }))
			throw new ConflictException('Adresse email invalide');

		const updating = await this.updating();
		const password = generatePassword();
		if (!updating)
		{
			await this.prisma.emailUpdating.create({
				data: {
					email: datas.email,
					password: password,
					user: {
						connect: {
							id: this.request['user_infos'].user.id
						}
					}
				}
			})
		}
		else
		{
			await this.prisma.emailUpdating.update({
				where: {
					id: updating.id
				},
				data: {
					email: datas.email,
					password: password
				}
			})
		}

		if (process.env.APP_ENV === 'dev')
			console.log(password);
		else
			await this.mailService.sendMail({
				to: datas.email,
				subject: 'Mise à jour de votre adresse email',
				text: ProfileMail.sendPassword(
					password,
					requestIp.getClientIp(this.request),
					useragent.parse(this.request.headers['user-agent']).toString(),
					useragent.parse(this.request.headers['user-agent']).os.toString(),
					useragent.parse(this.request.headers['user-agent']).device.toString()
				)
			});
		return {
			message: 'Email updated'
		};
	}

	async validateEmail(datas: ValidateEmailDto) {
		const updating = await this.updating();
		if (!updating)
			throw new NotFoundException('Pas de mise à jour en cours');
		if (updating.password !== datas.password)
			throw new BadRequestException('Mauvais mot de passe');
		if (updating.email !== datas.email)
			throw new BadRequestException('Adresse email invalide');
		if (await this.entity.findFirst({ where: { email: updating.email } }))
			throw new BadRequestException(`Impossible d'utiliser cette adresse`);

		await this.entity.update({
			where: {
				id: updating.userId
			},
			data: {
				email: updating.email
			},
		})
		await this.prisma.emailUpdating.delete({
			where: {
				id: updating.id
			}
		})

		return jwt.sign({
			datas: {
				id: this.request['user_infos'].user.id,
				email: updating.email,
				sessionId: this.request['user_infos'].hashSessionId,
				roles: this.request['user_infos'].roles,
				permissions: this.request['user_infos'].permissions
			}
		}, process.env.JWT_PRIVATE_KEY, {
			expiresIn: '10min',
			algorithm: 'ES512',
		});
	}

	async delete()
	{
		const user = this.request['user_infos'].user;

		await this.prisma.user.delete({
			where: {
				id: user.id
			}
		});

		return {
			message: 'User deleted'
		};
	}
}
