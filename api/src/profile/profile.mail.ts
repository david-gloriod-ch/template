export function sendPassword(password: string, ip: string, navigator: string, platform: string, device: string) {
	return `<h1>Code de confirmation de changement d'adresse: ${password}</h1>
		<p>Informations de la demande:</p>
		<ul>
			<li>Adresse IP: ${ip}</li>
			<li>Navigateur: ${navigator}</li>
			<li>Plateforme: ${platform}</li>
			<li>Appareil: ${device}</li>
		</ul>
		<p>Certaines informations peuvent ne pas être exactes</p>
	`;
}