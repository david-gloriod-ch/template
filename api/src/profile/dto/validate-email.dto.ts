import { IsNotEmpty, IsString, Length } from 'class-validator';
import { UpdateEmailDto } from './update-email.dto';

export class ValidateEmailDto extends UpdateEmailDto {
	@IsString({
		message: 'Mauvais format de mot de passe',
	})
	@Length(8, 8, {
		message: 'Longueur de mot de passe incorrecte',
	})
	@IsNotEmpty({
		message: 'Mot de passe nécessaire',
	})
	password: string;
}
