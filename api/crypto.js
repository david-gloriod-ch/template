const crypto = require('crypto');

const { privateKey, publicKey } = crypto.generateKeyPairSync('ec', {
	namedCurve: 'secp521r1'
});

const privateKeyString = privateKey.export({ type: 'pkcs8', format: 'pem' }).toString();
const publicKeyString = publicKey.export({ type: 'spki', format: 'pem' }).toString();

console.log('privateKey');
console.log(privateKeyString);
console.log();

console.log('publicKey');
console.log(publicKeyString);
console.log();
