-- AlterTable
ALTER TABLE "public"."Session" ADD COLUMN     "attempt" INTEGER NOT NULL DEFAULT 0,
ADD COLUMN     "locked" BOOLEAN NOT NULL DEFAULT false;
