# Template pour Site Internet

Ce template est conçu pour vous aider à démarrer rapidement le développement de votre site internet. Il inclut des fonctionnalités essentielles comme la gestion des rôles et des permissions, la gestion des utilisateurs, la pagination, les alertes, l'envoi d'emails, l'authentification, les logs, la gestion des cookies, et la modification des informations de profil.

## Fonctionnalités

- **Gestion des rôles et des permissions** : Attribuez des rôles spécifiques aux utilisateurs et définissez des permissions pour contrôler l'accès aux différentes parties de votre application.
- **Gestion des utilisateurs** : Ajoutez, modifiez et supprimez des utilisateurs facilement.
- **Système de pagination** : Facilitez la navigation à travers les grandes listes de données avec un système de pagination intégré.
- **Système d'alerte** : Affichez des messages d'alerte pour informer les utilisateurs des actions ou des événements importants.
- **Envoi d'emails** : Envoyez des emails aux utilisateurs pour des notifications, des confirmations ou tout autre besoin.
- **Système d'authentification** : Gérez l'inscription, la connexion et la déconnexion des utilisateurs.
- **Logs** : Gardez une trace des actions et des événements pour faciliter le débogage et la surveillance.
- **Gestion simplifiée des cookies** : Gérez facilement les cookies pour stocker les informations de session et autres données.
- **Logs** : Gardez une trace des actions et des événements pour faciliter le débogage et la surveillance.
- **Gestion simplifiée des cookies** : Gérez facilement les cookies pour stocker les informations de session et autres données.
- **Modification des informations de profil** : Permettez aux utilisateurs de mettre à jour leurs informations de profil, comme leur adresse email.

## Installation

Pour commencer avec ce template, suivez les étapes ci-dessous :

1. Créez et clonez un dépôt :

    ```bash
    git clone https://github.com/votre-utilisateur/votre-depot.git
    cd votre-depot
    ```

2. Ajoutez le lien du template en dépôt secondaire :

    ```bash
    git remote add template git@gitlab.com:david-gloriod-ch/template
    ```

3. Récupérez le template :
    ```bash
    git pull template template-main --no-rebase --allow-unrelated-histories
    ```

4. Lancez le script de configuration selon votre environnement :

    ```bash
    bash configure-dev.sh  # pour une configuration de développement
    bash configure-prod.sh  # pour une configuration de production
    ```

5. Lancez les containers Docker :

    ```bash
    npx sequelize db:migrate
    # ou
    npx prisma migrate deploy
    ```

6. Pour le mode développement:

    - Lancement de l'api: `nest start -w`
    - Lancement du client: `pnpm run dev`

## Principes

### Authentification
- La connexion / inscription est géré automatiquement en fonction de si l'utilisateur possède ou non déjà un compte
- L'authentification ne require pas de mot de passe mais seulement une adresse mail à la quel sera envoyé un code de connexion
- Par défaut toutes (ou presque) les routes demanderont une authentification. Si vous n'en souhaitez pas, vous pouvez utiliser le décorateurs `@Public()` présent dans le fichier `src/auth/decorators/public.decorator`

### Sessions
- Chaque instance de navigateur possède sa propre session, et chaque session possède son propre code de connexion

### Utilisateurs
- Chaque utilisateur possède un rôle portant le nom de leur id, ce que j'appelle les "rôle utilisateur"
- Chaque utilisateur peut posséder plusieurs rôles mais également plusieurs permissions qui lui sont propre

### Rôles
- Les rôles permettent de créer des groupes de permissions pour les associer plus facilement aux utilisateurs.
- Chaque rôle peut appartenir à plusieurs utilisateurs et posséder plusieurs permissions.

### Permissions
- Les permissions définissent les droits d'un utilisateur. Chaque permission a des niveaux de création, lecture, mise à jour et suppression.  
  Par conséquent, si vous souhaitez donner la permission de créer des utilisateurs à un rôle, vous pourrez créer une permission `utilisateurs` et sélectionner le droit de `créer`.
- Les niveaux de permissions sont :
  - create (1000)
  - read (0100)
  - update (0010)
  - delete (0001)
- Si un rôle ou un utilisateur a le droit de créer et de mettre à jour, il aura le niveau suivant : `1010`.

### Pagination
- La pagination permet de récupérer une quantité partielle des informations totales.

### Logs
- Les logs enregistrent chaque requête en base de données.

## Présentation du site

### Premier accès au site

http://<host>:3000 ou https://<host>

### Authentification

Accédez à la page de connexion / inscription `/auth/send-password`, connectez-vous avec votre adresse email afin d'avoir les droits administrateur, et:  
Pour le mode de développement le code s'affichera dans la console serveur. Production, il vous sera envoyé par email


## Administration

### Les rôles

1. **Création de votre premier rôle :**  
Rendez-vous sur la page des `Rôles` puis sur l'onglet `Créer`, saisissez ensuite les informations suivantes :  
- Le `nom` sera le nom du rôle tel que vous le lirez de manière "normale".
- Le `titre` est le terme "technique" utilisé dans le code, par exemple `@Roles(['administrator'])`.
- La `description` sert à décrire en quelques mots l'utilité du rôle.

  Vous pouvez **cocher** la case `Accéder au rôle après la création` si vous souhaitez directement accéder à sa gestion après sa création.

2. Assignation de permissions:  
   Une fois le rôle créé vous pourrez les assigner un niveau pour chaque permission. Si vous souhaitez faire en sorte qu'il permette de créer un utilisateur, alors allez sur la ligne `utilisateur` et **cochez** la case `créer` et **décochez** là pour lui supprimer la permission

3. 

### Système de pagination

Les grandes listes de données sont paginées par défaut. Vous pouvez ajuster le nombre d'éléments par page dans les paramètres.

### Système d'alerte

Utilisez la fonction `alert(message, type)` pour afficher des alertes. Les types d'alertes peuvent être `success`, `error`, `info`, ou `warning`.

### Envoi d'emails

Les emails peuvent être envoyés en utilisant la fonction `sendEmail(to, subject, message)`. Configurez vos paramètres SMTP dans le fichier `.env`.

### Système d'authentification

L'inscription, la connexion et la déconnexion sont gérées via des routes sécurisées. Les tokens de session sont stockés dans des cookies.

### Logs

Les actions et événements sont enregistrés dans les logs pour un suivi facile. Consultez les fichiers de logs dans le répertoire `logs`.

### Gestion des cookies

Utilisez les fonctions `setCookie(name, value, options)` et `getCookie(name)` pour gérer les cookies.

### Modification des informations de profil

Les utilisateurs peuvent mettre à jour leur adresse email et d'autres informations de profil depuis la page "Profil".

## Contribuer

Les contributions sont les bienvenues ! Pour soumettre une amélioration ou signaler un bug, veuillez ouvrir une issue ou soumettre une pull request.

## Licence

Ce projet est sous licence MIT. Voir le fichier [LICENSE](LICENSE) pour plus de détails.
