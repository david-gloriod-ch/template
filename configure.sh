create_slug() {
    local input="$1"
    local slug=$(echo "$input" | tr '[:upper:]' '[:lower:]')

    slug=$(echo "$slug" | sed 's/[^a-z0-9]/-/g')
    slug=$(echo "$slug" | sed 's/-\+/-/g')
    slug=$(echo "$slug" | sed 's/^-//' | sed 's/-$//')

    echo "$slug"
}

read_input() {
    local response

    while [ -z "$response" ]; do
        read response
    done

    echo "$response"
}


echo "=============================="
echo "Configuration de l'application"
echo "=============================="
echo ""

echo -n "Environnement <dev/prod>: "
APP_ENV=$(read_input)

echo -n "Domaine: "
DOMAIN=$(read_input)

echo -n "Nom de l'application: "
APP_NAME=$(read_input)



slug=$(create_slug "$APP_NAME")
echo -n "Slug ($slug) <y/n>: "
response=$(read_input)

if [ "$response" == "n" ]; then
    echo -n "Entrez le slug: "
    slug=$(read_input)
fi

echo ""
echo ""
echo "========================================="
echo "Configuration du service d'envoie d'email"
echo "========================================="
echo -n "SMTP: "
SMTP=$(read_input)

echo -n "Port: "
PORT=$(read_input)

echo -n "Email: "
EMAIL=$(read_input)

echo -n "Mot de passe de l'adresse mail: "
PASSWORD=$(read_input)

echo ""
echo ""
echo "================================="
echo "Configuration de l'administration"
echo "================================="
echo -n "Email pour le compte admin: "
ADMIN_EMAIL=$(read_input)

echo ""
echo ""
echo "================================="
echo "Configuration stripe"
echo "================================="
echo -n "Clé secrète: "
STRIPE_KEY_SECRET=$(read_input)

echo -n "Clé secrète du webhook: "
STRIPE_WEBHOOK_SECRET=$(read_input)

echo ""
echo ""
echo ""
echo "Configuration:"
echo "Environnement: $APP_ENV"
echo "Nom de l'application: $APP_NAME"
echo "Slug: $slug"
echo "Email: $EMAIL"
echo "Mot de passe: $PASSWORD"
echo "Email admin: $ADMIN_EMAIL"

mkdir -p configuration/tmp
mkdir -p database

cp configuration/files/.env.example configuration/tmp/.env
cp configuration/files/docker-compose.yml configuration/tmp/docker-compose.yml
cp configuration/files/nginx.conf configuration/tmp/nginx.conf
node configuration/scripts/js/crypto.js > configuration/tmp/keys

if [ ${APP_ENV} == "dev" ]; then
    PROTOCOL="http"
    API_URL="${PROTOCOL}://${DOMAIN}:8000"
    APP_URL="${PROTOCOL}://${DOMAIN}:3000"
else
    PROTOCOL="https"
    API_URL="${PROTOCOL}://${DOMAIN}/api"
    APP_URL="${PROTOCOL}://${DOMAIN}"
fi

APP_KEY=$(cat configuration/tmp/keys | grep APP_KEY | cut -d'=' -f2)
SALT_PASSWORD=$(cat configuration/tmp/keys | grep SALT_PASSWORD | cut -d'=' -f2)
SALT_SESSION_ID=$(cat configuration/tmp/keys | grep SALT_SESSION_ID | cut -d'=' -f2)
JWT_PRIVATE_KEY=$(cat configuration/tmp/keys | grep JWT_PRIVATE_KEY | cut -d'=' -f2 | sed 's|\\\\n>|\n|g' | sed 's|{EQUALS}|=|g')
JWT_PUBLIC_KEY=$(cat configuration/tmp/keys | grep JWT_PUBLIC_KEY | cut -d'=' -f2 | sed 's|\\\\n|\n|g' | sed 's|{EQUALS}|=|g')
POSTGRES_PASSWORD=$(cat configuration/tmp/keys | grep POSTGRES_PASSWORD | cut -d'=' -f2)
PGADMIN_DEFAULT_PASSWORD=$(cat configuration/tmp/keys | grep PGADMIN_DEFAULT_PASSWORD | cut -d'=' -f2)

sed -i "s|{{APP_NAME}}|${APP_NAME}|g" configuration/tmp/.env
sed -i "s|{{APP_ENV}}|${APP_ENV}|g" configuration/tmp/.env
sed -i "s|{{APP_URL}}|${APP_URL}|g" configuration/tmp/.env
sed -i "s|{{API_URL}}|${API_URL}|g" configuration/tmp/.env
sed -i "s|{{APP_KEY}}|${APP_KEY}|g" configuration/tmp/.env

sed -i "s|{{HOST}}|${DOMAIN}|g" configuration/tmp/.env

sed -i "s|{{SALT_PASSWORD}}|${SALT_PASSWORD}|g" configuration/tmp/.env
sed -i "s|{{SALT_SESSION_ID}}|${SALT_SESSION_ID}|g" configuration/tmp/.env

sed -i "s|{{JWT_PRIVATE_KEY}}|${JWT_PRIVATE_KEY}|g" configuration/tmp/.env
sed -i "s|{{JWT_PUBLIC_KEY}}|${JWT_PUBLIC_KEY}|g" configuration/tmp/.env

sed -i "s|{{SMTP}}|${SMTP}|g" configuration/tmp/.env
sed -i "s|{{PORT}}|${PORT}|g" configuration/tmp/.env
sed -i "s|{{MAIL_FROM}}|${EMAIL}|g" configuration/tmp/.env
sed -i "s|{{MAIL_PASSWORD}}|${PASSWORD}|g" configuration/tmp/.env

sed -i "s|{{ADMINISTRATOR_USER_EMAIL}}|${ADMIN_EMAIL}|g" configuration/tmp/.env
sed -i "s|{{PGADMIN_DEFAULT_PASSWORD}}|${PGADMIN_DEFAULT_PASSWORD}|g" configuration/tmp/.env

sed -i "s|{{STRIPE_KEY_SECRET}}|${STRIPE_KEY_SECRET}|g" configuration/tmp/.env
sed -i "s|{{STRIPE_WEBHOOK_SECRET}}|${STRIPE_WEBHOOK_SECRET}|g" configuration/tmp/.env

if [ "$APP_ENV" == "dev" ]; then
	sed -i "s|{{POSTGRES_HOST}}|localhost|g" configuration/tmp/.env
else
	sed -i "s|{{POSTGRES_HOST}}|${slug}_database|g" configuration/tmp/.env
fi
sed -i "s|{{PROJECT_SLUG}}|${slug}|g" configuration/tmp/.env

sed -i "s|{{PROJECT_SLUG}}|${slug}|g" configuration/tmp/docker-compose.yml
sed -i "s|{{PROJECT_SLUG}}|${slug}|g" configuration/tmp/nginx.conf
sed -i "s|{{DOMAIN}}|${DOMAIN}|g" configuration/tmp/nginx.conf

cp configuration/tmp/.env .env
cp configuration/tmp/.env api/.env
cp configuration/tmp/.env client/.env

cp configuration/tmp/docker-compose.yml docker-compose.yml

if [ "$APP_ENV" == "prod" ]; then
    sudo cp configuration/tmp/nginx.conf /etc/nginx/conf.d/${slug}.conf
fi

echo -n "Démarrer les dockers <y/n>: "
response=$(read_input)

if [ "$response" == "y" ] && [ "$APP_ENV" == "dev" ]; then
    sudo docker compose -f docker-compose.dev.yml up --build -d
elif [ "$response" == "y" ] && [ "$APP_ENV" == "prod" ]; then
    sudo docker compose up --build -d
fi

if [ "$APP_ENV" == "dev" ]; then
    echo -n "Installer les dépendances <y/n>: "
    response=$(read_input)

    if [ "$response" == "y" ]; then
        bash configuration/scripts/install.sh
    fi
fi

rm -rf configuration/tmp

echo "Configuration terminée"
