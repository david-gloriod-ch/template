if [ "$1" != "dev" ] && [ "$1" != "prod" ]; then
    echo "Usage: $0 <dev|prod>"
    exit 1
fi

if [ "$1" == "dev" ]; then
    sudo docker compose -f docker-compose.dev.yml down
    sudo docker compose -f docker-compose.dev.yml up --build -d
fi

if [ "$1" == "prod" ]; then
    sudo docker compose -f docker-compose.yml down
    sudo docker compose -f docker-compose.yml up --build -d
fi
