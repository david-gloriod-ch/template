export function setLayout(layout: string) {
	const state = useState<string>('layout');
	state.value = layout;
}

export function getLayout(): string {
	const layout = useState<string>('layout');
	return layout.value ?? 'default';
}
