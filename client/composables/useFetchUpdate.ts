async function fetchServer<T>(url: string, opts: any = {})
{
	const runtimeConfig = useRuntimeConfig();

	return await useFetch<T>(`${url}`, {
		...opts,
		onRequest({ options })
		{
			options.baseURL = runtimeConfig.public.API_URL,
			options.credentials = 'include';
		},
		async onResponseError({ request, response, options }) {
			useAlertStore().add({
				message: response._data.message,
				type: 'error',
			});
			if (response.status === 401 && !useRouter().currentRoute.value.path.startsWith(runtimeConfig.public.AUTH_PAGE)) {
				return window.location.href = `${runtimeConfig.public.LOGIN_PAGE}?next=${generate_next_route()}`;
			}
			else if (response.status === 403) {
				return useRouter().go(-1);
			}
			throw new Error(response._data.message);
		},
	})
	.then((data) => {
		return data.data.value as T;
	})
}
async function fetchClient<T>(url: string, opts: any = {})
{
	const runtimeConfig = useRuntimeConfig();

	return await $fetch<T>(`${url}`, {
		...opts,
		onRequest({ options })
		{
			options.baseURL = runtimeConfig.public.API_URL,
			options.credentials = 'include';
		},
		async onResponseError({ request, response, options }) {
			useAlertStore().add({
				message: response._data.message,
				type: 'error',
			});
			if (response.status === 401 && !useRouter().currentRoute.value.path.startsWith(runtimeConfig.public.AUTH_PAGE)) {
				return window.location.href = `${runtimeConfig.public.LOGIN_PAGE}?next=${generate_next_route()}`;
			}
			else if (response.status === 403) {
				return useRouter().go(-1);
			}
			throw new Error(response._data.message);
		},
	})
	.then((data) => {
		return data;
	})
}

export async function authorizedFetch<T>(url: string, opts: any = {})
{
	if (process.server)
		return fetchServer<T>(url, opts);
	return fetchClient<T>(url, opts);
}