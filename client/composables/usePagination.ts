export function setPageNumber(page: number)
{
	const state = useState<number>('page');
	state.value = page;
}

export function getPageNumber()
{
	return useState<number>('page').value ?? 1;
}

export function getPagination(max_page: number)
{
	const page = getPageNumber();
	const mid_page = Math.floor(max_page / 2);
	const pagination = [];
	for (let i = 1; i <= 3; i++)
	{
		pagination.push({
			page: i,
			active: i === page,
		});
	}
	return pagination;
}
