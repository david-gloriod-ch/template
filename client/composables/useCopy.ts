import { useAlertStore } from "../stores/Alert";

export function copyToClipboard(text: string) {
	var tempElement = document.createElement("textarea");
	tempElement.value = text;
	document.body.appendChild(tempElement);
	tempElement.select();
	document.execCommand("copy");
	document.body.removeChild(tempElement);

	useAlertStore().add({
		type: "success",
		message: "Copié dans le presse-papier",
	});
}
