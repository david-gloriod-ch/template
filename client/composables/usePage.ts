export function setPage(page: string)
{
	const state = useState<string>('page');
	state.value = page;
}

export function getPage()
{
	const state = useState<string>('page');
	return state.value;
}

export function isPage(page: string)
{
	const state = useState<string>('page');
	return state.value === page;
}
