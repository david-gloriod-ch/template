export function generate_next_route() {
	return btoa(useRouter().currentRoute.value.fullPath).replaceAll('=', '').replaceAll('/', '_').replaceAll('+', '-');
}

export function get_next_route() {
	return useRouter().currentRoute.value.query.next as string;
}

export function decode_next_route() {
	const route = get_next_route();
	return atob(route.replaceAll('_', '/').replaceAll('-', '+').padEnd(route.length + (4 - route.length % 4) % 4, '='));
}

export function has_next_route() {
	return useRouter().currentRoute.value.query.next !== undefined;
}

export function go_next_route(route?: string) {
	if (!route)
		route = useRouter().currentRoute.value.query.next as string;

	if (route)
		useRouter().push(decode_next_route());
}