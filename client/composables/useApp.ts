export function isInDevMode() {
	return useRuntimeConfig().public.APP_ENV === 'dev';
}

export function isInProdMode() {
	return useRuntimeConfig().public.APP_ENV !== 'dev';
}