import { defineStore } from "pinia";
import type { tUser } from "~/types/user";
import { DefaultUser } from "~/types/user";

export const useAuthStore = defineStore('auth', () => {
	const email = ref<string>('');
	const password = ref<string>('');

	return {
		email,
		password,

		isValidEmail() {
			return email.value.length > 0;
		},

		async sendPassword() {
			return await authorizedFetch<{ message: string }>('/auth/send-password', {
				method: 'POST',
				body: {
					email: email.value,
				},
			})
			.then(async (data) => {
				useAlertStore().add({
					message: data.message,
					type: 'success',
				});
				return await navigateTo(`/auth/validate-password${has_next_route() ? `?next=${get_next_route()}` : ''}`);
			})
		},

		async validatePassword() {
			return await authorizedFetch<{ message: string, authorization: string}>('/auth/validate-password', {
				method: 'POST',
				body: {
					email: email.value,
					password: password.value,
				},
			})
			.then(async (response) => {
				email.value = '';
				password.value = '';
				return window.location.href = has_next_route() ? decode_next_route() : '/';
			})
		},

		async logout() {
			return await authorizedFetch('/auth/logout', {
				method: 'POST',
			})
			.then(async () => {
				window.location.href = '/auth/send-password';
			})
		}
	};
});
