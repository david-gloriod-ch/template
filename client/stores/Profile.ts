import { defineStore } from "pinia";
import { CurrentUser } from "~/class/CurrentUser";
import { PermissionLevel } from "~/types/permission";
import type { tUser, tUserInfos } from "~/types/user";
import { DefaultUser } from "~/types/user";

export const useProfileStore = defineStore('profile', () => {
	const email = ref<string>('');
	const password = ref<string>('');
	const password_sent = ref<boolean>(false);

	const authStore = useAuthStore();

	const _profile = ref<tUser>({...DefaultUser});

	function getDatasFromJWT(token: string)
	{
		const payload = token.split('.')[1];
		const decoded = atob(payload);
		return JSON.parse(decoded).datas;
	}

	return {
		email,
		password,
		password_sent,

		get profile() {
			return _profile;
		},

		hasCookie() {
			return !!useCookie(`${useRuntimeConfig().public.APP_NAME.replaceAll(' ', '-').toLowerCase()}-authorization`).value;
		},

		fetch() {
			if (!this.hasCookie())
				return;

			const authorization_cookie = useCookie(`${useRuntimeConfig().public.APP_NAME.replaceAll(' ', '-').toLowerCase()}-authorization`);
			const authorization = authorization_cookie.value!;
			CurrentUser.getInstance().loadFromJWT(authorization);
			const user = getDatasFromJWT(authorization);
			_profile.value = user;
		},

		async updateEmail() {
			await authorizedFetch('/profile/update-email', {
				method: 'PATCH',
				body: {
					email: email.value,
				},
			})
			.then(async () => {
				password_sent.value = true;
				useAlertStore().add({
					message: 'Code envoyé',
					type: 'success',
				});
			});
		},

		async validateEmail() {
			return await authorizedFetch('/profile/validate-email', {
				method: 'PATCH',
				body: {
					email: email.value,
					password: password.value,
				},
			})
			.then(async () => {
				email.value = '';
				password.value = '';
				password_sent.value = false;
				await this.fetch();
				useAlertStore().add({
					message: 'Email modifié',
					type: 'success',
				});
			})
		},

		async delete() {
			return await authorizedFetch('/profile', {
				method: 'DELETE',
			})
			.then(async () => {
				await authStore.logout();
			})
		}
	};
});
