import { BaseManager } from "./Manager";

export abstract class Class<T> {
	protected _id?: string;
	protected _createdAt: Date;
	protected _updatedAt: Date;

	constructor(_id?: string, createdAt?: Date, updatedAt?: Date) {
		this._id = _id;
		this._createdAt = createdAt || new Date();
		this._updatedAt = updatedAt || new Date();
	}

	public get id(): string {
		return this._id!;
	}

	public get createdAt(): Date {
		return this._createdAt!;
	}

	public get updatedAt(): Date {
		return this._updatedAt!;
	}

	public abstract assignFrom(obj: any): void;

	public abstract toJSON(): any;

	public abstract clone(): T;
}