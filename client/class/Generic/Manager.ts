import type { Permission } from "../Permission/Permission";

export abstract class BaseManager<Manager, Class> {
	protected static _instances: {[key:string]:any} = {};
	private _page: Ref<number>;
	private _total: Ref<number>;
	private _max_page: Ref<number>;
	private _per_page: Ref<number>;

	protected constructor() {
		this._page = toRef(1);
		this._total = toRef(5);
		this._max_page = toRef(5);
		this._per_page = toRef(5);
	}

	public static getInstance() {
		throw new Error("Method not implemented.");
	}

	public get page(): Ref<number> {
		return this._page;
	}

	protected set page(value: number) {
		if (value < 1)
			throw new Error("Page must be greater than 0");
		if (value > this.max_page.value)
			throw new Error(`Page must be less than max_page (${this.max_page})`);
		this._page.value = value;
		useRouter().push({ query: { page: value } });
	}

	public get max_page(): Ref<number> {
		return toRef(this._max_page);
	}

	public set max_page(value: number) {
		this._max_page.value = value;
	}

	public get total(): Ref<number> {
		return this._total;
	}

	private get per_page(): Ref<number> {
		return this._per_page;
	}

	public set total(value: number) {
		this._total.value = value;
		this._max_page.value = Math.max(Math.ceil(value / this.per_page.value), 1);
	}

	public async create(datas: any): Promise<Class> {
		throw new Error("Method not implemented.");
	}

	public async update(id: string, datas: any): Promise<any> {
		throw new Error("Method not implemented.");
	}

	public async delete(id: string): Promise<Class> {
		throw new Error("Method not implemented.");
	}

	public async nextPage(): Promise<Class[]> {
		if (this.page.value < this.max_page.value) {
			this.page = this.page.value + 1;
		}
		return await this.fetch(this.page.value);
	}

	public async previousPage(): Promise<Class[]> {
		if (this.page.value > 1) {
			this.page = this.page.value - 1;
		}
		return await this.fetch(this.page.value);
	}

	public firstPage(): void {
		this.page = 1;
	}

	public lastPage(): void {
		this.page = this.max_page.value;
	}

	public setPage(page: number): void {
		this.page = page;
	}

	public abstract fetch(page: number): Promise<Class[]>;

	public toJSON() {
		return {
			page: this.page,
			total: this.total,
			max_page: this.max_page,
		};
	}
}
