import type { tCreateExample, tUpdateExample, tExample } from "~/types/example";
import { Example } from "./Example";
import { Role } from "../Role/Role";
import { BaseManager } from "../Generic/Manager";

export class ExampleManager extends BaseManager<ExampleManager, Example> {
	private _example?: Example;
	private _examples: Ref<Example[]>;

	protected constructor() {
		super();
		this._examples = toRef([]);
	}

	public static getInstance(): ExampleManager {
		if (!BaseManager._instances['ExampleManager']) {
			BaseManager._instances['ExampleManager'] = new ExampleManager();
		}
		
		return BaseManager._instances['ExampleManager'];
	}

	public get examples(): Ref<Example[]> {
		return this._examples;
	}

	public set examples(value: Example[]) {
		this._examples.value = value;
	}

	public get example(): Example | undefined {
		return this._example;
	}

	public set example(value: Example) {
		this._example = value;
	}

	public create(datas: tCreateExample): Promise<Example> {
		return authorizedFetch<tExample>(`/examples`, {
			method: 'POST',
			body: datas
		}).then((_) => {
			const example = new Example(_, _.id);
			this.examples.value.push(example);
			useAlertStore().add({
				type: 'success',
				message: 'Exemple créé',
			})
			return example;
		});
	}

	public async update(id: string, datas: tUpdateExample): Promise<Example> {
		return await authorizedFetch<tExample>(`/examples/${id}`, {
			method: 'PATCH',
			body: datas
		}).then((_: tExample) => {
			const pIndex = this.examples.value.findIndex(p => p.id === id);
			if (pIndex === -1)
				throw new Error(`Exemple introuvable`);
			const example = new Example(_, _.id);
			this.examples.value[pIndex] = example;
			return example;
		});
	}

	public async delete(id: string): Promise<Example> {
		return await authorizedFetch<tExample>(`/examples/${id}`, {
			method: 'DELETE'
		}).then((_) => {
			const sIndex = this.examples.value.findIndex(s => s.id === id);
			if (sIndex === -1)
				throw new Error(`Exemple introuvable`);
			const example = this.examples.value[sIndex];
			this.examples.value.splice(sIndex, 1);
			return example;
		});
	}

	public async fetch(page: number = 1): Promise<Example[]> {
		return await authorizedFetch<{examples:tExample[], total: number}>(`/examples?page=${page}`).then((response) => {
			this.examples.value = response.examples.map(example => new Example(example, example.id));
			this.total = response.total - 1;
			return this.examples.value;
		});
	}

	public async fetchById(id: string): Promise<Example> {
		return await authorizedFetch<tExample>(`/examples/${id}`).then((example) => {
			return new Example(example, id);
		});
	}
}
