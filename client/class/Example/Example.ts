import type { tCreateExample, tExample } from "~/types/example";
import { Class } from "../Generic/Class";

export class Example extends Class<Example> {
	private _name: string;

	constructor(example: tCreateExample, id?: string) {
		super(id);
		this._name = example.name;
	}

	public get name(): string {
		return this._name;
	}

	public set name(value: string) {
		this._name = value;
	}

	public async update(): Promise<this> {
		return await authorizedFetch<tExample>(`/examples/${this.id}`, {
			method: 'PATCH',
			body: {
				name: this.name
			}
		}).then((response: tExample) => {
			useAlertStore().add({
				type: 'success',
				message: `Exemple mis à jour`
			});
			this._id = response.id;
			return this;
		});
	}

	public async delete(): Promise<void> {
		await authorizedFetch(`/examples/${this.id}`, {
			method: 'DELETE'
		}).then(() => {
			useAlertStore().add({
				type: 'success',
				message: `Exemple supprimé`
			});
		});
	}

	public assignFrom(obj: Example): void {
		Object.assign(this, obj);
	}

	public toJSON(): tExample {
		return {
			id: this.id,
			name: this.name,
			createdAt: new Date(),
			updatedAt: new Date(),
		};
	}

	public clone(): Example {
		return new Example(this.toJSON());
	}
}
