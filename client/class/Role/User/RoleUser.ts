import type { tCreateRole, tRole, tRoleUser } from "~/types/role";
import type { Permission } from "../../Permission/Permission";
import type { PermissionLevel, tPermissionLevel } from "~/types/permission";
import { User } from "../../User/User";
import type { tUser } from "~/types/user";
import { Class } from "~/class/Generic/Class";

export class RoleUser extends Class<RoleUser> {
	private _name: string;
	private _user: User;
	private _permissions: tPermissionLevel[];

	constructor(role: tRoleUser, id?: string) {
		super(id);
		this._name = role.name;
		this._user = new User(role.user, role.user.id);
		this._permissions = [];
	}

	public get name(): string {
		return this._name;
	}

	public get user(): User {
		return this._user;
	}

	public get permissions(): tPermissionLevel[] {
		return this._permissions;
	}

	public set permissions(value: tPermissionLevel[]) {
		this._permissions = value;
	}

	public hasPermission(permission: Permission, level: PermissionLevel): boolean {
		const permissionLevel = this.permissions.find(p => p.permissionId === permission.id);
		return permissionLevel ? !!(parseInt(permissionLevel.level, 2) & parseInt(level, 2)) : false;
	}

	public async togglePermission(permission: Permission, level: PermissionLevel): Promise<void> {
		if (!this._id)
			throw new Error(`La role n'a pas d'id`);

		if (!this.hasPermission(permission, level))
			await this.addPermission(permission, level);
		else
			await this.removePermission(permission, level);
	}

	public async addPermission(permission: Permission, level: PermissionLevel): Promise<void> {
		if (!this._id)
			throw new Error(`La role n'a pas d'id`);

		await authorizedFetch<tPermissionLevel>(`/roles/${this._id}/permissions`, {
			method: 'POST',
			body: {
				permissionId: permission.id,
				level: level
			}
		}).then((response: tPermissionLevel) => {
			useAlertStore().add({
				type: 'success',
				message: `Permission ajoutée`
			});
			const index = this.permissions.findIndex(p => p.permissionId === permission.id);
			if (index === -1)
				this.permissions.push(response);
			else
				this.permissions[index] = response;
		});
	}

	public async removePermission(permission: Permission, level: PermissionLevel): Promise<void> {
		if (!this._id)
			throw new Error(`La role n'a pas d'id`);

		await authorizedFetch<tPermissionLevel>(`/roles/${this._id}/permissions`, {
			method: 'DELETE',
			body: {
				permissionId: permission.id,
				level: level
			}
		}).then((response: tPermissionLevel) => {
			useAlertStore().add({
				type: 'success',
				message: `Permission retirée`
			});
			const index = this.permissions.findIndex(p => p.permissionId === permission.id);
			if (index === -1)
				this.permissions.push(response);
			else
				this.permissions[index] = response;
		});
	}

	public assignFrom(obj: RoleUser): void {
		Object.assign(this, obj);
	}

	public toJSON(): tRoleUser {
		return {
			id: this.id,
			name: this.name,
			permissions: this.permissions,
			user: this.user.toJSON(),
		};
	}

	public clone(): RoleUser {
		return new RoleUser(this.toJSON());
	}
}
