import type { tRole, tRoleUser } from "~/types/role";
import { RoleUser } from "./RoleUser";
import { BaseManager } from "~/class/Generic/Manager";
import type { tPermissionLevel } from "~/types/permission";
import { Role } from "../Role";

export class RoleUserManager extends BaseManager<RoleUserManager, RoleUser> {
	private _roles: RoleUser[];

	private constructor() {
		super();
		this._roles = [];
	}

	public static getInstance(): RoleUserManager {
		if (!BaseManager._instances['RoleUserManager']) {
			BaseManager._instances['RoleUserManager'] = new RoleUserManager();
		}

		return BaseManager._instances['RoleUserManager'];
	}

	public get roles(): RoleUser[] {
		return this._roles;
	}

	public set roles(value: RoleUser[]) {
		this._roles = value;
	}

	public async fetch(page = 1): Promise<RoleUser[]> {
		return await authorizedFetch<{roles:tRoleUser[], total: number}>(`/roles/roles-users?page=${page}`).then((response) => {
			this.roles = response.roles.map(role => new RoleUser(role, role.id));
			this.total = response.total - 1;
			return this.roles;
		});
	}
}
