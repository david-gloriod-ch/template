import type { tCreateRole, tRole, tRoleUser, tUpdateRole } from "~/types/role";
import { Role } from "./Role";
import { BaseManager } from "../Generic/Manager";

export class RoleManager extends BaseManager<RoleManager, Role> {
	private _role?: Role;
	private _roles: Ref<Role[]>;

	protected constructor() {
		super();
		this._roles = toRef([]);
	}

	public static getInstance(): RoleManager {
		if (!BaseManager._instances['RoleManager']) {
			BaseManager._instances['RoleManager'] = new RoleManager();
		}

		return BaseManager._instances['RoleManager'];
	}

	public get roles(): Ref<Role[]> {
		return this._roles;
	}

	public set roles(value: Role[]) {
		this._roles.value = value;
	}

	public get role(): Role | undefined {
		return this._role;
	}

	public set role(value: Role) {
		this._role = value;
	}

	public async create(datas: tCreateRole): Promise<Role> {
		return await authorizedFetch<tRole>(`/roles`, {
			method: 'POST',
			body: datas
		}).then((_) => {
			const role = new Role(_, _.id);
			this.roles.value.push(role);
			useAlertStore().add({
				type: 'success',
				message: 'Role créé',
			})
			return role;
		});
	}

	public async update(id: string, datas: tUpdateRole): Promise<Role> {
		return await authorizedFetch<tRole>(`/roles/${id}`, {
			method: 'PATCH',
			body: datas
		}).then((_: tRole) => {
			const pIndex = this.roles.value.findIndex(p => p.id === id);
			if (pIndex === -1)
				throw new Error(`Role introuvable`);
			this.roles.value[pIndex] = new Role(_, id);
			useAlertStore().add({
				type: 'success',
				message: 'Role modifié',
			})
			return this.roles.value[pIndex];
		});
	}

	public async delete(id: string): Promise<Role> {
		return await authorizedFetch<tRole>(`/roles/${id}`, {
			method: 'DELETE'
		}).then((_) => {
			const pIndex = this.roles.value.findIndex(p => p.id === id);
			if (pIndex === -1)
				throw new Error(`Role introuvable`);
			const role = this.roles.value[pIndex];
			this.roles.value.splice(pIndex, 1);
			useAlertStore().add({
				type: 'success',
				message: 'Role supprimé',
			})
			return role;
		});
	}

	public add(role: Role): void {
		if (!role.id)
			throw new Error(`La role n'a pas d'id`);

		this.roles.value.push(role);
	}

	public remove(role: Role): void {
		if (!role.id)
			throw new Error(`La role n'a pas d'id`);

		const index = this.roles.value.findIndex(p => p.id === role.id);
		if (index === -1)
			throw new Error(`Role introuvable`);
		this.roles.value.splice(index, 1);
	}

	public async fetch(page = 1): Promise<Role[]> {
		return await authorizedFetch<{roles:tRole[], total: number}>(`/roles?page=${page}`).then((response) => {
			this.roles = response.roles.map(role => new Role(role, role.id));
			this.total = response.total - 1;
			return this.roles.value;
		});
	}

	public async fetchPermissions(page = 1): Promise<Role[]> {
		return await authorizedFetch<{roles:tRole[], total: number}>(`/roles/permissions?page=${page}`).then((response) => {
			this.roles = response.roles.map(_ => {
				const role = new Role(_, _.id)
				role.permissions = _.permissions;
				return role;
			});
			this.total = response.total - 1;
			return this.roles.value;
		});
	}

	public async fetchById(id: string): Promise<Role> {
		return await authorizedFetch<tRole>(`/roles/${id}`).then((role) => {
			return new Role(role, id);
		});
	}
}
