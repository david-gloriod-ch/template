import type { tCreateRole, tRole } from "~/types/role";
import { RoleManager } from "./Manager";
import { Permission } from "../Permission/Permission";
import type { PermissionLevel, tPermissionLevel } from "~/types/permission";
import { User } from "../User/User";
import type { tUser } from "~/types/user";
import { Class } from "../Generic/Class";

export class Role extends Class<Role> {
	private _title: string;
	private _name: string;
	private _description: string;
	private _users: User[];
	private _permissions: tPermissionLevel[];

	constructor(role: tCreateRole, id?: string) {
		super(id);
		this._title = role.title;
		this._name = role.name;
		this._description = role.description;
		this._users = [];
		this._permissions = [];
	}

	public get title(): string {
		return this._title;
	}

	public set title(value: string) {
		this._title = value;
	}

	public get name(): string {
		return this._name;
	}

	public set name(value: string) {
		this._name = value;
	}

	public get description(): string {
		return this._description;
	}

	public set description(value: string) {
		this._description = value;
	}

	public get users(): User[] {
		return this._users;
	}

	public set users(value: User[]) {
		this._users = value;
	}

	public get permissions(): tPermissionLevel[] {
		return this._permissions;
	}

	public set permissions(value: tPermissionLevel[]) {
		this._permissions = value;
	}

	public set(_role: tRole): void {
		this._id = _role.id;
		this._title = _role.title;
		this._name = _role.name;
		this._description = _role.description;
		this._permissions = _role.permissions ?? [];
		this._users = _role.users?.map(user => new User(user, user.id)) ?? [];
	}

	public async fetchUsers(page: number = 1): Promise<User[]> {
		if (!this._id)
			throw new Error(`La role n'a pas d'id`);

		await authorizedFetch<{users: tUser[], total: number}>(`/roles/${this._id}/users?page=${page}`).then((response) => {
			this._users = response.users.map(user => new User(user, user.id));
		});

		return this._users;
	}

	public async fetchPermissions(): Promise<tPermissionLevel[]> {
		if (!this._id)
			throw new Error(`La role n'a pas d'id`);

		return await authorizedFetch<tPermissionLevel[]>(`/roles/${this._id}/permissions`).then((response) => {
			this.permissions = response;
			return response;
		});
	}

	public hasPermission(permission: Permission, level: PermissionLevel): boolean {
		const permissionLevel = this.permissions.find(p => p.permissionId === permission.id);
		return permissionLevel ? !!(parseInt(permissionLevel.level, 2) & parseInt(level, 2)) : false;
	}

	public async togglePermission(permission: Permission, level: PermissionLevel): Promise<void> {
		if (!this._id)
			throw new Error(`La role n'a pas d'id`);

		if (!this.hasPermission(permission, level))
			await this.addPermission(permission, level);
		else
			await this.removePermission(permission, level);
	}

	public async addPermission(permission: Permission, level: PermissionLevel): Promise<void> {
		if (!this._id)
			throw new Error(`La role n'a pas d'id`);

		await authorizedFetch<tPermissionLevel>(`/roles/${this._id}/permissions`, {
			method: 'POST',
			body: {
				permissionId: permission.id,
				level: level
			}
		}).then((response: tPermissionLevel) => {
			useAlertStore().add({
				type: 'success',
				message: `Permission ajoutée`
			});
			const index = this.permissions.findIndex(p => p.permissionId === permission.id);
			if (index === -1)
				this.permissions.push(response);
			else
				this.permissions[index] = response;
		});
	}

	public async removePermission(permission: Permission, level: PermissionLevel): Promise<void> {
		if (!this._id)
			throw new Error(`La role n'a pas d'id`);

		await authorizedFetch<tPermissionLevel>(`/roles/${this._id}/permissions`, {
			method: 'DELETE',
			body: {
				permissionId: permission.id,
				level: level
			}
		}).then((response: tPermissionLevel) => {
			useAlertStore().add({
				type: 'success',
				message: `Permission retirée`
			});
			const index = this.permissions.findIndex(p => p.permissionId === permission.id);
			if (index === -1)
				this.permissions.push(response);
			else
				this.permissions[index] = response;
		});
	}

	public assignFrom(obj: Role): void {
		Object.assign(this, obj);
	}

	public toJSON(): tRole {
		return {
			id: this.id,
			title: this.title,
			name: this.name,
			description: this.description,
			permissions: this.permissions,
			users: [],
		};
	}

	public clone(): Role {
		const newRole = new Role(this.toJSON());
		newRole.permissions = this.permissions;
		newRole.users = this.users;
		return newRole;
	}
}
