import type { tSession } from "~/types/session";
import { Session } from "./Session";
import { Role } from "../Role/Role";
import { BaseManager } from "../Generic/Manager";

export class SessionManager extends BaseManager<SessionManager, Session> {
	private _session?: Session;
	private _sessions: Ref<Session[]>;

	protected constructor() {
		super();
		this._sessions = toRef([]);
	}

	public static getInstance(): SessionManager {
		if (!BaseManager._instances['SessionManager']) {
			BaseManager._instances['SessionManager'] = new SessionManager();
		}

		return BaseManager._instances['SessionManager'];
	}

	public get sessions(): Ref<Session[]> {
		return this._sessions;
	}

	public set sessions(value: Session[]) {
		this._sessions.value = value;
	}

	public get session(): Session | undefined {
		return this._session;
	}

	public set session(value: Session) {
		this._session = value;
	}

	public async delete(id: string): Promise<Session> {
		return await authorizedFetch<tSession>(`/sessions/${id}`, {
			method: 'DELETE'
		}).then((_) => {
			const sIndex = this.sessions.value.findIndex(s => s.id === id);
			if (sIndex === -1)
				throw new Error(`Session introuvable`);
			const session = this.sessions.value[sIndex];
			this.sessions.value.splice(sIndex, 1);
			return session;
		});
	}

	public async fetch(page: number = 1): Promise<Session[]> {
		return await authorizedFetch<{sessions:tSession[], total: number}>(`/sessions?page=${page}`).then((response) => {
			this.sessions.value = response.sessions.map(session => new Session(session, session.id));
			this.total = response.total;
			return this.sessions.value;
		});
	}
}
