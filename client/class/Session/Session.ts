import type { tSession } from "~/types/session";
import { SessionManager } from "./Manager";
import type { Permission } from "../Permission/Permission";
import type { PermissionLevel, tPermissionLevel } from "~/types/permission";
import { Role } from "../Role/Role";
import type { tRole } from "~/types/role";
import { Class } from "../Generic/Class";

export class Session extends Class<Session> {
	private _navigator: string;
	private _platform: string;
	private _device: string;
	private _address: string;

	constructor(session: tSession, id?: string) {
		super(id, session.createdAt, session.updatedAt);
		this._navigator = session.navigator;
		this._platform = session.platform;
		this._device = session.device;
		this._address = session.address;
	}

	public get navigator(): string {
		return this._navigator;
	}

	public get platform(): string {
		return this._platform;
	}

	public get device(): string {
		return this._device;
	}

	public get address(): string {
		return this._address;
	}

	public assignFrom(obj: Session): void {
		Object.assign(this, obj);
	}

	public toJSON(): tSession {
		return {
			id: this.id,
			createdAt: this.createdAt,
			updatedAt: this.updatedAt,
			navigator: this.navigator,
			platform: this.platform,
			device: this.device,
			address: this.address,
			isCurrent: false,
		};
	}

	public clone(): Session {
		return new Session(this.toJSON());
	}
}
