import type { tPermission } from "~/types/permission";
import { Class } from "../Generic/Class";

export class Permission extends Class<Permission> {
	private _title: string;
	private _name: string;
	private _description: string;

	constructor(permission: tPermission, id?: string) {
		super(id);
		this._title = permission.title;
		this._name = permission.name;
		this._description = permission.description;
	}

	public get title(): string {
		return this._title;
	}

	public set title(value: string) {
		this._title = value;
	}

	public get name(): string {
		return this._name;
	}

	public set name(value: string) {
		this._name = value;
	}

	public get description(): string {
		return this._description;
	}

	public set description(value: string) {
		this._description = value;
	}

	public assignFrom(obj: Permission): void {
		Object.assign(this, obj);
	}

	public set(permission: tPermission): void {
		this._id = permission.id;
		this._title = permission.title;
		this._name = permission.name;
		this._description = permission.description;
	}

	public toJSON(): tPermission {
		return {
			id: this.id,
			title: this.title,
			name: this.name,
			description: this.description,
			users: [],
			roles: []
		};
	}

	public clone(): Permission {
		return new Permission(this.toJSON());
	}
}
