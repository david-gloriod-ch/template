import type { tCreatePermission, tPermission, tUpdatePermission } from "~/types/permission";
import { Permission } from "./Permission";
import { BaseManager } from "../Generic/Manager";

export class PermissionManager extends BaseManager<PermissionManager, Permission> {
	private _permission?: Permission;
	private _permissions: Ref<Permission[]>;

	protected constructor() {
		super();
		this._permissions = toRef([]);
	}

	public static getInstance(): PermissionManager {
		if (!BaseManager._instances['PermissionManager']) {
			BaseManager._instances['PermissionManager'] = new PermissionManager();
		}

		return BaseManager._instances['PermissionManager'];
	}

	public get permissions(): Ref<Permission[]> {
		return this._permissions;
	}

	public set permissions(value: Permission[]) {
		this._permissions.value = value;
	}

	public get permission(): Permission {
		return this._permission!;
	}

	public set permission(value: Permission) {
		this._permission = value;
	}

	public async create(datas: tCreatePermission): Promise<Permission> {
		return await authorizedFetch<tPermission>(`/permissions`, {
			method: 'POST',
			body: datas
		}).then((_) => {
			const permission = new Permission(_, _.id);
			this.permissions.value.push(permission);
			useAlertStore().add({
				type: 'success',
				message: 'Permission créée',
			})
			return permission;
		});
	}

	public async update(id: string, datas: tUpdatePermission): Promise<Permission> {
		return await authorizedFetch<tPermission>(`/permissions/${id}`, {
			method: 'PATCH',
			body: datas
		}).then((_: tPermission) => {
			const pIndex = this.permissions.value.findIndex(p => p.id === id);
			if (pIndex === -1)
				throw new Error(`Permission introuvable`);
			this.permissions.value[pIndex].set(_);
			useAlertStore().add({
				type: 'success',
				message: 'Permission mise à jour',
			})
			return this.permissions.value[pIndex];
		});
	}

	public async delete(id: string): Promise<Permission> {
		return await authorizedFetch<tPermission>(`/permissions/${id}`, {
			method: 'DELETE'
		}).then((_: tPermission) => {
			const index = this.permissions.value.findIndex(p => p.id === _.id);
			if (index === -1)
				throw new Error(`Permission introuvable`);
			const permission = this.permissions.value[index];
			this.permissions.value.splice(index, 1);
			useAlertStore().add({
				type: 'success',
				message: 'Permission supprimée',
			})
			return permission;
		});
	}

	public async fetch(page = 1): Promise<Permission[]> {
		return await authorizedFetch<{permissions:tPermission[], total: number}>(`/permissions?page=${page}`).then((response) => {
			this.permissions = response.permissions.map(permission => new Permission(permission, permission.id));
			this.total = response.total - 1;
			return this.permissions.value;
		});
	}

	public async fetchById(id: string): Promise<Permission> {
		return await authorizedFetch<tPermission>(`/permissions/${id}`).then((_) => {
			const permission = new Permission(_, id);
			this.permission = permission;
			return permission;
		});
	}
}
