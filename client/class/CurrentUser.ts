import type { PermissionLevel } from "~/types/permission";
import { DEFAULT_ACCESS } from "./Permission/Config";

interface IJWTPayload {
	datas: IPayloadDatas;
}

interface IPayloadDatas {
	id: string;
	email: string;
	sessionId: string;
	roles: {[key: string]: string};
	permissions: {[key: string]: string};
}

type tUser = {
	id: string;
	email: string;
	roles: {[key: string]: string};
	permissions: {[key: string]: string};
};

export class CurrentUser {
	private static _instance: CurrentUser;

	private _user: Ref<tUser>;

	private constructor()
	{
		this._user = toRef({
			id: '',
			email: '',
			roles: {},
			permissions: {}
		});
		
	}

	public get user(): Ref<tUser> {
		return this._user;
	}

	public set user(value: tUser) {
		this._user.value = value;
	}

	// Fixer le problème de référence réactive

	public static getInstance(): CurrentUser
	{
		if (!CurrentUser._instance)
			CurrentUser._instance = new CurrentUser();
		return CurrentUser._instance;
	}

	public isAuthenticated(): boolean {
		return !!this.user.value.email;
	}

	public loadFromJWT(_token: string): void {
		const payload = _token.split('.')[1];
		const decoded = atob(payload);
		const parsed: IJWTPayload = JSON.parse(decoded);
		const datas = parsed.datas;
		this.user = datas;
	}

	public hasDefaultPermission(): boolean {
		return !!this.user.value.permissions[DEFAULT_ACCESS];
	}

	public hasPermission(_permission: string, level: PermissionLevel): boolean {
		if (!this.user.value)
			return false;
		return this.hasDefaultPermission() || !!(parseInt(this.user.value.permissions[_permission], 2) & parseInt(level, 2));
	}
}
