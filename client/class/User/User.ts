import type { tCreateUser, tUser } from "~/types/user";
import type { Permission } from "../Permission/Permission";
import type { tPermissionLevel } from "~/types/permission";
import { Role } from "../Role/Role";
import type { tRole } from "~/types/role";
import { DefaultCreateRole } from "~/types/role";
import { Class } from "../Generic/Class";

export class User extends Class<User> {
	private _email: string;
	private _role: Role;
	private _roles: Role[];
	private _permissions: Permission[];

	constructor(user: tCreateUser, id?: string) {
		super(id);
		this._email = user.email;
		this._roles = [];
		this._permissions = [];
		this._role = new Role({...DefaultCreateRole});
	}

	public get email(): string {
		return this._email;
	}

	public set email(value: string) {
		this._email = value;
	}

	public get roles(): Role[] {
		return this._roles!;
	}

	public set roles(value: Role[]) {
		this._roles = value;
	}

	public get role(): Role {
		return this._role;
	}

	public get permissions(): Permission[] {
		return this._permissions!;
	}

	public set permissions(value: Permission[]) {
		this._permissions = value;
	}

	public hasRole(_role: Role): boolean {
		return this.roles.some(role => role.id === _role.id);
	}

	public async fetchRoles(page = 1): Promise<Role[]> {
		if (!this._id)
			throw new Error(`La user n'a pas d'id`);

		await authorizedFetch<tRole[]>(`/users/${this._id}/roles?page=${page}`).then((response: tRole[]) => {
			this.roles = response.map(role => new Role(role, role.id));
		});

		return this.roles;
	}

	public async fetchRole(): Promise<Role> {
		if (!this._id)
			throw new Error(`La user n'a pas d'id`);

		return await authorizedFetch<tRole>(`/users/${this._id}/role`).then((response: tRole) => {
			this.role.set(response);
			return this.role;
		});
	}

	public async fetchPermissions(): Promise<tPermissionLevel[]> {
		if (!this._id)
			throw new Error(`La user n'a pas d'id`);

		const role = await this.fetchRole();

		return await role.fetchPermissions();
	}

	public async toggleRole(_role: Role): Promise<void> {
		if (!this._id)
			throw new Error(`La user n'a pas d'id`);
		
		if (!this.hasRole(_role))
			await this.addRole(_role);
		else
			await this.removeRole(_role);
	}

	public async addRole(_role: Role): Promise<void> {
		if (!this._id)
			throw new Error(`La user n'a pas d'id`);

		await authorizedFetch<tRole>(`/users/${this._id}/role`, {
			method: 'POST',
			body: {
				roleId: _role.id
			}
		}).then((response: tRole) => {
			useAlertStore().add({
				type: 'success',
				message: `Rôle ajoutée`
			});
			this.roles.push(new Role(response, response.id));
		});
	}

	public async removeRole(_role: Role): Promise<void> {
		if (!this._id)
			throw new Error(`La user n'a pas d'id`);

		await authorizedFetch<tRole>(`/users/${this._id}/role`, {
			method: 'DELETE',
			body: {
				roleId: _role.id
			}
		}).then((response: tRole) => {
			useAlertStore().add({
				type: 'success',
				message: `Rôle retirée`
			});
			const index = this.roles.findIndex(r => r.id === _role.id);
			this.roles.splice(index, 1);
		});
	}

	public async update(): Promise<this> {
		return await authorizedFetch<tUser>(`/users/${this.id}`, {
			method: 'PATCH',
			body: {
				email: this.email
			}
		}).then((response: tUser) => {
			useAlertStore().add({
				type: 'success',
				message: `Utilisateur mis à jour`
			});
			this._id = response.id;
			return this;
		});
	}

	public async delete(): Promise<void> {
		await authorizedFetch(`/users/${this.id}`, {
			method: 'DELETE'
		}).then(() => {
			useAlertStore().add({
				type: 'success',
				message: `Utilisateur supprimé`
			});
		});
	}

	public assignFrom(obj: User): void {
		Object.assign(this, obj);
	}

	public toJSON(): tUser {
		return {
			id: this.id,
			email: this.email,
			roles: this.roles.map(role => role.toJSON()),
		};
	}

	public clone(): User {
		return new User(this.toJSON());
	}
}
