import type { tCreateUser, tUpdateUser, tUser } from "~/types/user";
import { User } from "./User";
import { Role } from "../Role/Role";
import { BaseManager } from "../Generic/Manager";

export class UserManager extends BaseManager<UserManager, User> {
	private _user?: User;
	private _users: Ref<User[]>;

	protected constructor() {
		super();
		this._users = toRef([]);
	}

	public static getInstance(): UserManager {
		if (!BaseManager._instances['UserManager']) {
			BaseManager._instances['UserManager'] = new UserManager();
		}
		
		return BaseManager._instances['UserManager'];
	}

	public get users(): Ref<User[]> {
		return this._users;
	}

	public set users(value: User[]) {
		this._users.value = value;
	}

	public get user(): User | undefined {
		return this._user;
	}

	public set user(value: User) {
		this._user = value;
	}

	public create(datas: tCreateUser): Promise<User> {
		return authorizedFetch<tUser>(`/users`, {
			method: 'POST',
			body: datas
		}).then((_) => {
			const user = new User(_, _.id);
			this.users.value.push(user);
			useAlertStore().add({
				type: 'success',
				message: 'Utilisateur créé',
			})
			return user;
		});
	}

	public async update(id: string, datas: tUpdateUser): Promise<User> {
		return await authorizedFetch<tUser>(`/users/${id}`, {
			method: 'PATCH',
			body: datas
		}).then((_: tUser) => {
			const pIndex = this.users.value.findIndex(p => p.id === id);
			if (pIndex === -1)
				throw new Error(`Utilisateur introuvable`);
			const user = new User(_, _.id);
			this.users.value[pIndex] = user;
			return user;
		});
	}

	public async delete(id: string): Promise<User> {
		return await authorizedFetch<tUser>(`/users/${id}`, {
			method: 'DELETE'
		}).then((_) => {
			const sIndex = this.users.value.findIndex(s => s.id === id);
			if (sIndex === -1)
				throw new Error(`Utilisateur introuvable`);
			const user = this.users.value[sIndex];
			this.users.value.splice(sIndex, 1);
			return user;
		});
	}

	public async fetch(page: number = 1): Promise<User[]> {
		return await authorizedFetch<{users:tUser[], total: number}>(`/users?page=${page}`).then((response) => {
			this.users.value = response.users.map(user => new User(user, user.id));
			this.total = response.total - 1;
			return this.users.value;
		});
	}

	public async fetchRoles(page = 1): Promise<User[]> {
		return await authorizedFetch<{users:tUser[], total: number}>(`/users/roles?page=${page}`).then((response) => {
			this.users.value = response.users.map(_ => {
				const user = new User(_, _.id)
				user.roles = _.roles.map(role => new Role(role, role.id));
				return user;
			});
			this.total = response.total - 1;
			return this.users.value;
		});
	}

	public async fetchById(id: string): Promise<User> {
		return await authorizedFetch<tUser>(`/users/${id}`).then((user) => {
			return new User(user, id);
		});
	}

	public async findByEmail(email: string, page: number = 1): Promise<User[]> {
		await authorizedFetch<{users: tUser[], total: number}>(`/users/search?email=${email}&page=${page}`).then((response) => {
			this.users = response.users.map(user => new User(user, user.id));
			this.total = response.total;
		});
		return this.users.value;
	}
}
