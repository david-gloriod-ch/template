export default defineNuxtRouteMiddleware((to, from) => {
	if (to.path.startsWith('/admin/'))
		setPageLayout('admin')
	else
		setPageLayout('default')
})