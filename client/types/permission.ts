import type { tRole } from "./role";
import type { tUser } from "./user";

export type tCreatePermission = {
	title: string;
	name: string;
	description: string;
}

export type tUpdatePermission = tCreatePermission;

export type tPermission = tCreatePermission & {
	id: string;
	users: tUser[];
	roles: tRole[];
}

export type tPermissionLevel = {
	role?: tRole;
	roleId?: string;
	permission?: tPermission;
	permissionId?: string;
	level: string;

}

export enum PermissionLevel {
	CREATE = '1000',
	READ = '0100',
	UPDATE = '0010',
	DELETE = '0001',
}

export const DefaultCreatePermission: tCreatePermission = {
	title: '',
	name: '',
	description: '',
}

export const DefaultPermission: tPermission = {
	id: '',
	...DefaultCreatePermission,
	users: [],
	roles: [],
}
