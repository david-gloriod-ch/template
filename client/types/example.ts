import type { tGeneric } from "./generics";
import { DefaultGeneric } from "./generics";

export type tCreateExample = {
	name: string;
};

export type tUpdateExample = tCreateExample;

export type tExample = tGeneric & tCreateExample;

export const DefaultCreateExample: tCreateExample = {
	name: '',
};

export const DefaultExemple: tExample = {
	...DefaultGeneric,
	name: '',
};