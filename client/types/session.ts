import { DefaultGeneric, type tGeneric } from "./generics";

export type tSession = tGeneric & {
	navigator: string;
	platform: string;
	device: string;
	address: string;
	isCurrent: boolean;
}

export const DefaultSession: tSession = {
	...DefaultGeneric,
	navigator: '',
	platform: '',
	device: '',
	address: '',
	isCurrent: false,
}
