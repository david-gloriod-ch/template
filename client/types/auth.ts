export type tSendPassword = {
	email: string;
}

export type tValidatePassword = tSendPassword & {
	password: string;
}

export const DefaultSendPassword: tSendPassword = {
	email: '',
}

export const DefaultValidatePassword: tValidatePassword = {
	...DefaultSendPassword,
	password: '',
}
