export type tGeneric = {
	id: string;
	createdAt: Date;
	updatedAt: Date;
}

export const DefaultGeneric: tGeneric = {
	id: '',
	createdAt: new Date(),
	updatedAt: new Date(),
}