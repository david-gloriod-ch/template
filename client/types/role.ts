import type { tPermission, tPermissionLevel } from "./permission";
import type { tUser } from "./user";

export type tCreateRole = {
	title: string;
	name: string;
	description: string;
}

export type tUpdateRole = tCreateRole;

export type tRole = tCreateRole & {
	id: string;
	users: tUser[];
	permissions: tPermissionLevel[];
}

export type tRoleUser = Omit<tRole, 'users' | 'title' | 'description'> & {user: tUser};

export type tRolePermission = Omit<tRole, 'permissions'> & {permission: tPermission};

export const DefaultCreateRole: tCreateRole = {
	title: '',
	name: '',
	description: '',
}

export const DefaultRole: tRole = {
	id: '',
	...DefaultCreateRole,
	users: [],
	permissions: [],
}

