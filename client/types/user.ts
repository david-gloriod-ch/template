import { type tGeneric } from './generics';
import type { tRole } from './role';

export type tCreateUser = {
	email: string;
}

export type tUpdateUser = tCreateUser;

export type tUser = tCreateUser & {
	id: string;
	roles: tRole[];
}

export const DefaultUser: tUser = {
	id: '',
	email: '',
	roles: [],
}

export type tUserInfos = {
	number_of_sessions: number;
}