// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
	devtools: { enabled: true },
	css: ['~/assets/styles/main.scss'],
	app: {
		head: {
			title: process.env.APP_NAME,
			link: [
				{
					rel: 'stylesheet',
					href: 'https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.3/font/bootstrap-icons.min.css',
				},
				{
					rel: 'stylesheet',
					href: 'https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css',
					integrity: 'sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH',
					crossorigin: 'anonymous'
				},
			],
			script: [
				{
					src: 'https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js',
					integrity: 'sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz',
					crossorigin: 'anonymous'
				}
			]
		}
	},
	runtimeConfig: {
		public: {
			API_URL: process.env.API_URL,
			APP_URL: process.env.APP_URL,
			APP_NAME: process.env.APP_NAME,
			APP_ENV: process.env.APP_ENV,
			LOGIN_PAGE: '/auth/send-password',
			AUTH_PAGE: '/auth/',
		}
	},
	modules: [
		'@pinia/nuxt',
		'@vite-pwa/nuxt',
	],
	pwa: {
		manifest: {
			name: process.env.APP_NAME,
			short_name: process.env.APP_NAME,
			description: process.env.APP_NAME,
			icons: [
				{
					src: 'logo-square.webp',
					sizes:'192x192',
					type: 'image/webp'
				}
			],
			
		},
	},
})
